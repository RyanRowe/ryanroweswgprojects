﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TagsSpike.Startup))]
namespace TagsSpike
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TagsSpike.Models;

namespace TagsSpike.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult GetTags()
        {
            var repo = new TagRepo();
            var tags = repo.GetTags();
            return View(tags);
        }

        //[HttpGet]
        //public ActionResult AddTags()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult AddTags()
        //{

        //}



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TagsSpike.Models
{
    public class TagRepo
    {
        private List<Tag> _tags;

        public TagRepo()
        {
            if (_tags == null || _tags.Count == 0)
            {
                _tags = new List<Tag>()
                {
                    new Tag() {Id = 1, Name = "Gotcha" },
                    new Tag() {Id = 2, Name = "Commas" },
                    new Tag() {Id = 3, Name = "Kamakazee" }
                };
            }
        }

        public void AddTag(Tag tag)
        {
            _tags.Add(tag);
        }

        public List<Tag> GetTags()
        {
            return _tags;
        }

        public Tag GetTagById(Tag tag)
        {
           var returntag = _tags.FirstOrDefault(t => t.Id == tag.Id);
           return returntag;
        }



    }
}
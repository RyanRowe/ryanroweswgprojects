﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        int sum = 1;   
        int randomNumber;

        static void Main(string[] args)
        {
            Program p = new Program();
            p.PickNumber();
            Console.ReadLine();
        }

        public void PickNumber()

        {
            Random random = new Random();
            randomNumber = random.Next(1, 21);
            //randomNumber = 1; //test for first test

            Console.Write("Welcome to Guessing Game!!!! Please Tell Me Your Name: ");
            string inputName = Console.ReadLine();
            int validInput = 0;
            Console.Write($"Welcome {inputName}! Try to Guess the Hidden Number Between 1 and 20 (You can type Q anytime to quit):");
            string inputNumber = Console.ReadLine();
            bool isValid;
            bool wonGame;
            do
            {
                do
                {
                    //guessing
                    if (inputNumber.ToLower() == "q")
                    {
                        Console.Write("Thank You For Playing ('Press Enter to Exit Game')");
                        return;
                    }
                    isValid = int.TryParse(inputNumber, out validInput);

                    if (!isValid || (validInput < 1 || validInput > 20))
                    {
                        Console.Write("Please Try Again, a Number Between 1 and 20: ");
                        inputNumber = Console.ReadLine();
                    }

                } while (!isValid || (validInput < 1 || validInput > 20));

                wonGame = GuessNumber(validInput);
                if (!wonGame)
                {
                    inputNumber = Console.ReadLine();
                }
            } while (!wonGame);
            

            
        }




        public bool GuessNumber(int validInput)
        {
            if (validInput == randomNumber && sum == 1)
            {
                Console.Write($"Congrats!!! You guesssed {randomNumber} on the first try!!(Press Enter to Quit)");
                return true;
            }
            if (validInput == randomNumber)
            {
                Console.WriteLine("It's About Time you Newb!!!");
                Console.WriteLine($"Number of Guesses {sum} (Press Enter to Quit)");
                return true;
            }
            
            sum = sum + 1;
            Console.Write("Wrong Guess Please Try Again: ");
            return false;
        
                
        }






    }
}


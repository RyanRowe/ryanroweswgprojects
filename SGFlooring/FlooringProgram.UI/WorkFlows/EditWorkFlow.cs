﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using FlooringProgram.Models.Responses;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.WorkFlows
{
    public class EditWorkFlow
    {
        Helpers helpers = new Helpers();
        OrderManager manager = new OrderManager();
        DisplayWorkFlow dwf = new DisplayWorkFlow();
        ProductManager productManager = new ProductManager();
        StateManager stateManager = new StateManager();
        

        public void Execute()
        {
            var plr = productManager.GetProducts();
            var slr = stateManager.GetStates();
            if (!slr.Success || !plr.success)
            {
                Console.WriteLine($"Error!!!!!! No Data for State or Product!!!!!! Returning To Main Menu");
                Console.ReadLine();
                return;
            }
            Order order = new Order();            
            Console.Clear();
            Console.WriteLine("Edit Order");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
           // OrderResponse response = new OrderResponse();
            Console.Write("Please Enter a Date to Pull up Orders for Edit: ");
            var validDate = helpers.GetDateTimeFromUser();
            var results = manager.GetOrdersByDate(validDate);
            dwf.DisplayOrders(results, validDate);

            //this if statement will just break out of the method once
            //an alert pops up that there is no valid dates with orders
            if (!results.Success)
            {
                return;
            }

            Console.WriteLine("");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
            Console.Write("Please enter Order Number to Edit: ");
            int orderNumberToPull = helpers.GetIntergerFromUser();
            //OrderResponse orderToDisplay = new OrderResponse();

            //this makes sure the number pulled cannot be a number
            //populated on the list
            if (orderNumberToPull > results.FilteredOrders.Count)
            {
               // orderToDisplay.Success = false;
               // orderToDisplay.Message = "That order does not Exist!!";
                Console.WriteLine("That order does not Exist!!");
                return;
            }

            //this grabs an order response and displays the orderToDisplay.FilterOrderObject
            OrderResponse orderToDisplay = manager.GetOrder(results.FilteredOrders, orderNumberToPull);

            Screens.DisplayOrder(orderToDisplay.FilteredOrder);

           
            //this prompts a user to create a new object and brings in the old
            //object that was pulled up above(orderToDisplay.FilteredOrder)
            var newOrder = PromptUserForUpdates(orderToDisplay.FilteredOrder, plr, slr);



            Console.Clear();

            //this will keep the same order number if the dates are the same
            //and if its not it will use my addordernumbermethod which
            //goes the the max order number and adds 1 and makes that new
            //order number
            if (newOrder.OrderDate == orderToDisplay.FilteredOrder.OrderDate)
            {
                newOrder.OrderNumber = orderToDisplay.FilteredOrder.OrderNumber;
            }
            else
            {
                newOrder = manager.AddOrderNumber(newOrder);
            }
           
            Screens.DisplayOrder(newOrder);
            Console.Write("Are you sure you want to Edit? (Y/N) : ");

            while (true)
            {
                string input = helpers.GetStringFromUser();

                switch (input.ToUpper())
                {
                    case "Y":
                        //I dont think I need this method it still works
                        //because remove is in the update method in the repos
                        //manager.RemoveOrder(orderToDisplay.FilteredOrder);
                        ProcessOrder(newOrder, orderToDisplay.FilteredOrder);
                        Console.WriteLine("Changes Saved.....");
                        return;                   
                    case "N":
                        Console.WriteLine("Aborting.....");
                        return;
                    default:
                        Console.Write("Y or N please!!!! : ");
                        break;
                }
            }


          
        }
        /// <summary>
        /// this method just passes in the order and will change the properties
        /// or keep them the same if nullorwhitespace(enter) is inputed
        /// these are seperate methods in my helper class compared 
        /// to my original helpers that requrie validation before being passed
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private Order PromptUserForUpdates(Order order, ProductListResponse plr, StateListResponse slr)
        {
            Order newOrder = new Order();

            Console.WriteLine("Press Enter to Keep an existing value!!!!!");
            newOrder.OrderNumber = order.OrderNumber;
            Console.Write($"Enter New Date Please : ");
            newOrder.OrderDate = helpers.GetDateTimeFromUserForEdit(order);
            Console.Write("New Customer Name : ");
            newOrder.CustomerName = helpers.GetStringFromUserforCustomerNameEdit(order);
            Console.Clear();
            Screens.DisplayOrderStates(slr);
            Console.Write("New State : ");
            newOrder.OrderState = helpers.GetStateFromUserForEdit(order);
            Console.Clear();
            Screens.DisplayOrderProducts(plr);
            Console.Write("New Product : ");
            newOrder.OrderProduct = helpers.GetProductFromUserForEdit(order);
            Console.Clear();
            Screens.DisplayOrder(newOrder);
            Console.Write("New Order Area : ");
            newOrder.OrderArea = helpers.GetAreaFromUserForEdit(order);
            manager.CalculateAddOrder(newOrder);

            return newOrder;
        }
        /// <summary>
        /// this method will bring in an order use the update
        /// order method in the bll to bring back and order response 
        /// and create and delete new objects for updating
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private Order ProcessOrder(Order orderToAdd, Order orderToDelete)
        {
            var manager = new OrderManager();
            var response = manager.UpdateOrder(orderToAdd, orderToDelete);

            if (!response.Success)
            {
                Console.WriteLine("Something went wrong!!!!!");
                Console.WriteLine(response.Message);
                Console.ReadLine();
            }

            return response.FilteredOrder;
        }
     

        
    }
}

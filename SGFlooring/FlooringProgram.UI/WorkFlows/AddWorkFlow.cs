﻿using FlooringProgram.Models.Models;
using FlooringProgram.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Data.Factories;
using FlooringProgram.Models.Responses;

namespace FlooringProgram.UI.WorkFlows
{
    public class AddWorkFlow
    {


        Helpers helpers = new Helpers();
        OrderManager orderManager = new OrderManager();
        StateManager stateManager = new StateManager();
        ProductManager productmanager = new ProductManager();
        //ProductManager productManager = new ProductManager();

        public void Execute()
        {
            var productlist = productmanager.GetProducts();
            var stateList = stateManager.GetStates();
            //stateList.Success = true;
            if(!stateList.Success || !productlist.success)
            {
                ErrorLog.WriteLog("A State or Product were not Loaded");
                Console.WriteLine($"Error!!!!!! No Data For State or Product!!!!!! Returning To Main Menu");
                Console.ReadLine();
                return;
            }

            Console.Clear();
            Console.WriteLine("Add Orders");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine();
            Order order = orderManager.CalculateAddOrder(GetOrderToAdd(stateList, productlist));
            Screens.DisplayOrder(order);
            Console.WriteLine("Confirm this order Y/N?");
            string confirm = helpers.GetStringFromUser().ToUpper();
            ConfirmAddingOrder(order, confirm);
        }

        public Order GetOrderToAdd(StateListResponse statelist, ProductListResponse productlist)
        {
                     
            Order order = new Order();
           
            Console.Write("Date for Order (Use Common Date Formats) : ");
            order.OrderDate = helpers.GetDateTimeFromUser();
            Console.Write("Name for the Order: ");
            order.CustomerName = helpers.GetStringFromUser();
            Console.Clear();
            Screens.DisplayOrderProducts(productlist);
            Console.Write("Product Type: ");         
            order.OrderProduct = helpers.GetProductByTypeFromUser();
            Console.Clear();
            Screens.DisplayOrderStates(statelist);
            Console.Write("State by Abbreviation or Full Name : ");
            order.OrderState = helpers.GetStateFromUser();
            order = orderManager.AddOrderNumber(order);
            Screens.DisplayOrder(order);
            Console.Write("Total Area needed to floor (sq foot) : ");
            order.OrderArea = helpers.GetAreaFromUser();           
            return order;
        }
         
        public Order ConfirmAddingOrder(Order order, string input)
        {
            if (input == "Y" || input == "YES")
            {              
                orderManager.AddConfirmedOrder(order);
                Console.WriteLine($"Added Confirmed Order!!!!");
            }
            else
            {
                Console.WriteLine("Aborting Add Back to Main Menu");
            }
            return order;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models.Responses;
using FlooringProgram.UI.Utilities;
using FlooringProgram.Models.Models;

namespace FlooringProgram.UI.WorkFlows
{
    public class RemoveWorkFlow
    {
        DisplayWorkFlow dwf = new DisplayWorkFlow();
        Helpers helpers = new Helpers();
        OrderManager manager = new OrderManager();


        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("Remove Order");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
            OrderResponses response = new OrderResponses();
            OrderManager manager = new OrderManager();
            Console.Write("Please Enter a Date to Pull up Orders for Removal: ");
            var validDate = helpers.GetDateTimeFromUser();
            var results = manager.GetOrdersByDate(validDate);
            dwf.DisplayOrders(results, validDate);

            if (results.Success == false)
            {
                return;
            }

            Console.WriteLine("");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
            Console.Write("Please Choose Order to Remove by Number in Parentheses : ");
            int orderNumberRemove = helpers.GetIntergerFromUser();
            OrderResponse order = new OrderResponse();

            if (orderNumberRemove > results.FilteredOrders.Count)
            {
                response.Success = false;
                response.Message = "That order does not Exist!!";
                Console.WriteLine(response.Message);
                return;
            }

            order = manager.GetOrder(results.FilteredOrders, orderNumberRemove);

            if (order.Success == false)
            {
                Console.WriteLine(order.Message);
                return;
            }
              
            Console.WriteLine("");
            Console.Clear();
            Screens.DisplayOrder(order.FilteredOrder);
            Console.WriteLine("Remove This Order? Y/N : ");
           
            while (true)
            {
                string input = helpers.GetStringFromUser();

                switch (input.ToUpper())
                {
                    case "Y":
                        Console.WriteLine("Deleting.....");
                        manager.RemoveOrder(order.FilteredOrder);
                        return;

                    case "N":
                        Console.WriteLine("Aborting.....");
                        return;
                       

                    default:
                        Console.Write("Y or N please!!!! : ");
                        break;
                }
            }

        }

        
    }
}

﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using FlooringProgram.Models.Models;
using FlooringProgram.Data.Repositories.Mock;
using FlooringProgram.Models.Responses;
using FlooringProgram.UI.Utilities;

namespace FlooringProgram.UI.WorkFlows
{
    public class DisplayWorkFlow
    {
        Helpers helpers = new Helpers();

        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("Display Orders");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
            Console.Write("Please Enter Date for Orders (Example 12/12/2012): ");
            OrderResponses response = new OrderResponses();
            OrderManager manager = new OrderManager();
            var validDate = helpers.GetDateTimeFromUser();
            var results = manager.GetOrdersByDate(validDate);
        
            DisplayOrders(results, validDate);

            if (results.Success == false)
            {
                return;
            }

            Console.WriteLine("");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine("");
            Console.Write("Please Choose Individual order to Display by Number in Parentheses : ");

            int orderNumberDisplay = helpers.GetIntergerFromUser();
            OrderResponse order = new OrderResponse();

            if (orderNumberDisplay > results.FilteredOrders.Count)
            {
                response.Success = false;
                response.Message = "That order does not Exist!!";
                Console.WriteLine(response.Message);
                ErrorLog.WriteLog("User tried to Enter a List Number not availible on that Date");
                return;
            }

            order = manager.GetOrder(results.FilteredOrders, orderNumberDisplay);
            Screens.DisplayOrder(order.FilteredOrder);
            Console.WriteLine("Press Enter to Exit to Main Menu");
        }


        /// <summary>
        /// I want this to display the orders from the response object and date pertaining to it
        /// </summary>
        /// <param name="results"></param>
        /// <param name="date"></param>
        public void DisplayOrders(OrderResponses results, DateTime date)
        {
            if (!results.Success)
            {
                Console.WriteLine("There is no Orders Corresponding to that date!");
                ErrorLog.WriteLog("User Entered an Unavailible Date To Display Orders by Date");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine($"Results for {date:d}");
                Console.WriteLine(helpers.seperator);
                Console.WriteLine("");
                int i = 1;
                foreach (var result in results.FilteredOrders)
                {               
                    Console.WriteLine($" ({i}) Order #{result.OrderNumber} || Customer Name: {result.CustomerName} || Total Area: {result.OrderArea} sq feet || Total Cost: {result.OrderTotal:c}");
                    i++;
                }

                
            }
        }

    }
}

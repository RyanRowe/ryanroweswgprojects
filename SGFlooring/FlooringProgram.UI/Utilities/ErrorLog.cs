﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.UI.Utilities
{
    public class ErrorLog
    {

        public static void WriteLog(string prompt)
        {
            DateTime date = DateTime.Now;
            using (
                StreamWriter sw =
                    new StreamWriter(
                        @"C:\_repos\ryan-rowe-individual-work\SGFlooring\FlooringProgram.UI\bin\Debug\DataFiles\ErrorLogging.txt",
                        true))
            {
                sw.WriteLine($"Error: Date={date.ToShortDateString()}-{date.ToShortTimeString()} // Prompt={prompt}");
            }

        }
    }
}




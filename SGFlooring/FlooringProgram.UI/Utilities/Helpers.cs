﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;
using FlooringProgram.BLL;
using FlooringProgram.Models.Responses;

namespace FlooringProgram.UI.Utilities
{
    public class Helpers
    {
        public string seperator = "=============================================";

        //GET ANY DATE/TIMES THAT THE USER MIGHT 
        // GetDateTimeFromUser();
        // GetDateTimeFromUser(true); // WON'T WORK FOR YOUR REQUIREMENTS
        // GetDateTimeFromUser(true, "10/16/2016");
        public DateTime GetDateTimeFromUser()
        {
            DateTime date = new DateTime();
            bool valid = false;
            while (!valid)
            {
                string input = Console.ReadLine();
                valid = DateTime.TryParse(input, out date);
                if (valid == false)
                {
                    Console.Write("Not a valid Date, please check your format: ");
                    ErrorLog.WriteLog("User Entered Wrong Date");

                }
            }
            return date;
        }

        //GET ANY STRINGS THAT MIGHT BE NEEDED FOR THE USER
        public string GetStringFromUser()
        {
            string result = "";
            while (true)
            {
                result = Console.ReadLine();
                if (!string.IsNullOrEmpty(result))
                {
                    break;
                }
                Console.Write("Please Enter Something: ");
                ErrorLog.WriteLog("User Entered an Invalid String");
            }
            return result;

        }

        //GET ANY DECIMAL THE USER MIGHT NEED
        public decimal GetDecimalFromUser()
        {
            string input = Console.ReadLine();
            decimal validInput;
            bool valid = decimal.TryParse(input, out validInput);
            while (!valid)
            {
                Console.Write("Not a valid input, please try again");
                input = Console.ReadLine();
                valid = decimal.TryParse(input, out validInput);
                ErrorLog.WriteLog("User Entered an Invalid Input for a Decimal");

            }
            return validInput;


        }

        //GET ANY INTEGER THE USER MIGHT NEED
        public int GetIntergerFromUser()
        {
            string input = Console.ReadLine();
            int validInput;
            bool valid = int.TryParse(input, out validInput);
            while (!valid)
            {
                Console.Write("Not a valid input, please try again : ");
                input = Console.ReadLine();
                valid = int.TryParse(input, out validInput);
                ErrorLog.WriteLog("User Entered an Invalid interger");

            }
            return validInput;
        }

        //GET ANY PRODUCT THAT WE HAVE IN OUR PRODUCTS AVAILIBLE
        public Product GetProductByTypeFromUser()
        {
            string productType = GetStringFromUser();
            ProductManager manager = new ProductManager();
            ProductResponses response = manager.GetProductByName(productType);

            while (response.Success == false)
            {
                Console.Write("This Product Type is not availible, Try Again: ");
                productType = GetStringFromUser();
                response = manager.GetProductByName(productType);
                ErrorLog.WriteLog("User Entered an Invalid Product");

            }
            return response.Products;
        }

        //GET ANY STATE THAT WE HAVE IN OUR AVAILIBLE STATES
        public State GetStateFromUser()
        {
            
            string stateInput = GetStringFromUser();
            StateManager manager = new StateManager();
            StateResponses response = manager.GetStateByName(stateInput);

            while (response.Success == false)
            {
                Console.Write("This State is not availible, Try Again: ");
                stateInput = GetStringFromUser();
                response = manager.GetStateByName(stateInput);
                ErrorLog.WriteLog("User Entered an Invalid State");

            }
            return response.States;
        }

        //GET AN AREA FOR THE ORDER MAKE SURE IT IS ABOVE 0
        public decimal GetAreaFromUser()
        {
            string input = Console.ReadLine();
            decimal validInput;
            bool valid = decimal.TryParse(input, out validInput);
            valid = (validInput > 0);
            while (!valid)
            {
                Console.Write("Not a valid input, please try again : ");
                input = Console.ReadLine();
                valid = decimal.TryParse(input, out validInput);
                valid = (validInput > 0);
                ErrorLog.WriteLog("User Entered an Invalid Area for Floor");

            }
            return validInput;
        }

        //will get datetime from user and keep old date time if user left empty
        public DateTime GetDateTimeFromUserForEdit(Order order)
        {
            DateTime date = new DateTime();
            bool valid = false;
            while (!valid)
            {
                string input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    date = order.OrderDate;
                    return date;
                }
                valid = DateTime.TryParse(input, out date);
                if (valid == false)
                {
                    Console.Write("Not a valid Date, please check your format: ");


                }
            }
            return date;
        }

        //will get area from user and if nothing keep old area
        public decimal GetAreaFromUserForEdit(Order order)
        {
            decimal validInput = 0;
            bool valid = false;
            while (!valid)
            {
                string input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    validInput = order.OrderArea;
                    return validInput;
                }

                valid = decimal.TryParse(input, out validInput);
                valid = (validInput > 0);

                if (valid == false)
                {
                    Console.Write("Not a valid input, please try again");
                    input = Console.ReadLine();
                    valid = decimal.TryParse(input, out validInput);
                    valid = (validInput > 0);
                    ErrorLog.WriteLog("User Entered an Invalid Area");
                }


            }
            return validInput;

        }

        //get customer name and if empty keep old customer name
        public string GetStringFromUserforCustomerNameEdit(Order order)
        {
            string result = "";
            {
                result = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(result))
                {
                    result = order.CustomerName;
                    return result;
                }              

            }
            return result;
        }

        //get state object and keep same state object if empty input
        public State GetStateFromUserForEdit(Order order)
        {
            string stateString = Console.ReadLine();
            State state = new State();
            if (string.IsNullOrWhiteSpace(stateString))
            {
                state = order.OrderState;
                return state;
            }
            StateManager manager = new StateManager();
            StateResponses response = manager.GetStateByName(stateString);

            while (response.Success == false)
            {
                Console.Write("This State is not availible, Try Again: ");
                stateString = GetStringFromUser();
                response = manager.GetStateByName(stateString);
                ErrorLog.WriteLog("User Entered an Invalid State");
            }
            return response.States;
        }

        //get product object and keep same product object if empty input
        public Product GetProductFromUserForEdit(Order order)
        {
            string productType = Console.ReadLine();
            Product product = new Product();
            if (string.IsNullOrWhiteSpace(productType))
            {
                product = order.OrderProduct;
                return product;
            }
            //if product is going change, it will go through this code below.
            ProductManager manager = new ProductManager();
            ProductResponses response = manager.GetProductByName(productType);

            while (response.Success == false)
            {
                Console.Write("This Product Type is not availible, Try Again: ");
                productType = GetStringFromUser();
                response = manager.GetProductByName(productType);
                ErrorLog.WriteLog("User Entered an Invalid Product");
            }
            return response.Products;
        }

       
    }
 }

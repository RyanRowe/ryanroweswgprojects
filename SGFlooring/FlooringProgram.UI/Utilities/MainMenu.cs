﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;
using FlooringProgram.UI.Utilities;
using FlooringProgram.UI.WorkFlows;

namespace FlooringProgram.UI
{
    public class MainMenu
    {
        
        

        private static void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("SG Flooring Management System");
            Console.WriteLine("=============================================");
            Console.WriteLine("1. Display Orders ");
            Console.WriteLine("2. Add Order ");
            Console.WriteLine("3. Remove Order ");
            Console.WriteLine("4. Edit Order");
            Console.WriteLine();
            Console.WriteLine("Q - Quit");
            Console.WriteLine();
            Console.WriteLine("=============================================");
            Console.Write("Enter Choice: ");
        }

        private static bool ProcessChoice()
        {
            string input = Console.ReadLine();
           


            switch (input.ToUpper())
            {
                case "1":
                    var displayWF = new DisplayWorkFlow();
                    displayWF.Execute();
                    break;
                case "2":
                    var AddWF = new AddWorkFlow();
                    AddWF.Execute();            
                    break;
                case "3":
                    var RemoveWF = new RemoveWorkFlow();                  
                    RemoveWF.Execute();                
                    break;
                case "4":
                 //   Order order = new Order();
                    var EditWF = new EditWorkFlow();
                    EditWF.Execute();
                    break;
                case "Q":
                    Console.WriteLine("Have a good day");                  
                    return false;
                default:
                    Console.WriteLine("That is not a valid choice. Press Enter to try again please...");  
                    ErrorLog.WriteLog("User Tried to Enter an Invalid Choice at Main Menu");       
                    break;
            }
            return true;
        }

        public static void Show()
        {
            bool continueRunning = true;

            while (continueRunning)
            {              
                DisplayMenu();
                continueRunning = ProcessChoice();        
                string input = Console.ReadLine();
            }


        }

    }
}


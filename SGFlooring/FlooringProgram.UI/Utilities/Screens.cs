﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;
using FlooringProgram.Models.Responses;

namespace FlooringProgram.UI.Utilities
{
    static class Screens
    {

        //just pass an object in to get all of its properties to display on the console
        public static void DisplayOrder(Order order)
        {
            Helpers helpers = new Helpers();
            Console.Clear();
            Console.WriteLine("Order");
            Console.WriteLine(helpers.seperator);
            Console.WriteLine($"Order Number : {order.OrderNumber}");
            Console.WriteLine($"Date : {order.OrderDate:d}");
            Console.WriteLine($"Customer Name : {order.CustomerName}");
            Console.WriteLine($"Product Type : {order.OrderProduct.ProductType}");
            Console.WriteLine($"Total Area Requested : {order.OrderArea} sq ft");
            Console.WriteLine($"Cost of Product Labor : {order.OrderProduct.CostPerSquareFootLabor:c} per sq ft");
            Console.WriteLine($"Cost of Product Materials : {order.OrderProduct.CostPerSquareFootMaterials:c} per sq ft");
            Console.WriteLine($"State : {order.OrderState.Name}");
            Console.WriteLine($"State Abbv : {order.OrderState.Abbreviation}");
            Console.WriteLine($"State Tax : {order.OrderState.TaxRate:p}");
            Console.WriteLine($"Total Labor Cost : {order.TotalLaborCost:c}");
            Console.WriteLine($"Total Material Cost : {order.TotalMaterialCost:c}");
            Console.WriteLine($"Total Tax : {order.TotalTax:c}");
            Console.WriteLine($"Order Total : {order.OrderTotal:c}");
            Console.WriteLine("");
        }

        public static void DisplayOrderStates(StateListResponse response)
        {
            //Helpers helper = new Helpers();
            Console.WriteLine("                     Order States Availble for Customer Purchase                                ");
            Console.WriteLine("");
            foreach (var state in response.StateList)
            {
                
                Console.WriteLine($"State Name : {state.Name}------------Abb : {state.Abbreviation}");
                Console.WriteLine("");

            }

        }

        public static void DisplayOrderProducts(ProductListResponse response)
        {
           // Helpers helper = new Helpers();
            Console.WriteLine("                      Products in Stock                                  ");
            Console.WriteLine("");
            foreach (var product in response.ProductList)
            {
                Console.WriteLine($"Product Type : {product.ProductType}\nCostPerSquareFoot Materials : {product.CostPerSquareFootMaterials}\n" +
                                  $"CostPerSquareFoot Labor : {product.CostPerSquareFootLabor}");
                Console.WriteLine("");

            }

        }


    }
}

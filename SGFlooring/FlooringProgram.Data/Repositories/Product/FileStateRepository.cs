﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Repositories.Product
{
    public class FileStateRepository : IStateRepository
    {

        //new state field
        private List<State> _states;

        //constructor since these are read onlys
        public FileStateRepository()
        {
            if (_states == null)
            {
                //temporary list that will be = to the field _states in the end
               // List<State> states = new List<State>();

                //using streamReader and opening the file give to us for project
                using (

                    StreamReader sr =
                        File.OpenText(
                            @"C:\_repos\ryan-rowe-individual-work\SGFlooring\FlooringProgram.UI\bin\Debug\DataFiles\States.txt"))

                { 
                    sr.ReadLine();

                    List<State> states = new List<State>();

                    string line = "";

                    while ((line = sr.ReadLine()) !=null)
                    {
                        string[] inputColumns = line.Split(',');
                        State state = new State()
                        {
                            Abbreviation = inputColumns[0],
                            Name = inputColumns[1],
                            //since the taxrate in the files are in whole number form
                            //I have to divide by 100 to get a decimal value to multiply
                            //in calculations
                            TaxRate = decimal.Parse(inputColumns[2])/100
                        };

                        states.Add(state);
                    }

                    _states = states;
                }
            }
        }

        public State LoadState(string input)
        {
            //default returns null if there is no objects, and will not use expections errors
            return _states.FirstOrDefault(s => (s.Abbreviation.ToUpper() == input.ToUpper()) || (s.Name.ToUpper() == input.ToUpper()));
        }

        public List<State> LoadStates()
        {
            return _states;
        }
    }
}

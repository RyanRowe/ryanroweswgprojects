﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;
using System.IO;

namespace FlooringProgram.Data.Repositories.Product
{
    public class FileOrderRepository : IOrderRepository

    {
        //new list to return at end
        private List<Order> _orders;
        //I will write this file on every new file to have a referral for a user who wants to 
        //read the actual file, my streamreader should skip the first line everytime it reads a
        //new file so I should put this as the first write on new files to add or edit to new dates
        private const string OrderHeader =
            "OrderNumber,OrderDate,CustomerName,StateAbb,StateName,StateTax,ProductType,MaterialCost,LaborCost,OrderArea,TotalMaterialCost,TotalLaborcost,TotalTax,OrderTotal";
        //this is the directory I want to use for my files this will be string interped with the 
        //split input date to string called fileDate
        //($"Orders_{dateParts[0]}{dateParts[1]}{dateParts[2]}.txt") will be fileDate
        //that added with the fileDirectory should make the correct filepath for reading/writing
        private const string fileDirectory =
           @"C:\_repos\ryan-rowe-individual-work\SGFlooring\FlooringProgram.UI\bin\Debug\DataFiles\";

        public List<Order> LoadOrders(DateTime orderDate)
        {
            //NameFileByDate will make a string for file name with orderDate passed in
            string orderFile = NameFileByDate(orderDate);

            //then we will pass and make a string with {fileDirectory}+{orderFile}
            string filePath = MakeFilePathFromNameFileByDate(orderFile);


            //is there a file name that mathches our filepath?
            if (File.Exists(filePath))
            {
                //list we are going to use and make it equal to _orders list field
                List<Order> orders = new List<Order>();

                //streamread class instance, File.OpenText lets the sr 
                //start reading the file
                using (StreamReader sr = File.OpenText($"{filePath}"))
                {

                    //skips first line since this will be show the properties and what order they 
                    //are in on the file
                    sr.ReadLine();

                    string input = "";

                    //this loop will keep going until the streamreader reads a line that is null
                    while ((input = sr.ReadLine()) != null)
                    {
                        //making a split array that will use the newOrder 
                        //Objects being passed in from file
                        //and seperate each property by commas
                        string[] inputColumns = input.Split(',');

                        //indexes for the inputColumns(order of properties on files)
                        var newOrder = new Order()
                        {
                            OrderNumber = int.Parse(inputColumns[0]),
                            OrderDate = DateTime.Parse(inputColumns[1]),
                            CustomerName = inputColumns[2],
                            OrderState = new State()
                            {
                                Abbreviation = inputColumns[3],
                                Name = inputColumns[4],
                                TaxRate = decimal.Parse(inputColumns[5]),
                            },
                            OrderProduct = new Models.Models.Product()
                            {
                                ProductType = inputColumns[6],
                                CostPerSquareFootMaterials = decimal.Parse(inputColumns[7]),
                                CostPerSquareFootLabor = decimal.Parse(inputColumns[8]),

                            },
                            OrderArea = decimal.Parse(inputColumns[9]),
                            TotalMaterialCost = decimal.Parse(inputColumns[10]),
                            TotalLaborCost = decimal.Parse(inputColumns[11]),
                            TotalTax = decimal.Parse(inputColumns[12]),
                            OrderTotal = decimal.Parse(inputColumns[13])
                        };

                        //adding each split newOrder to order list
                        orders.Add(newOrder);
                    }
                    //makes the _orders field equal to the objects created in the while loop above
                    _orders = orders;


                }


            }
            //not sure I need this, want to return something 
            //if there is not a file with that file name
            return _orders;
        }

        //public List<Order> LoadOrders(DateTime orderDate)
        //{
        //    return _orders.Where(o => o.OrderDate.CompareTo(orderDate) == 0).ToList();
        //}

        public Order GetOrderByNumber(List<Order> orders, int choice)
        {
            Order order = new Order();

            order = orders[choice - 1];

            return order;
        }

        public void RemoveOrder(Order order)
        {
            var orders = LoadOrders(order.OrderDate);
            
            var orderToRemove = 
                orders.FirstOrDefault(o => o.OrderNumber == order.OrderNumber && o.OrderDate == order.OrderDate);

            orders.Remove(orderToRemove);
                
            if (orders.Count == 0)
            {
                string orderFile = NameFileByDate(order.OrderDate);

                string filePath = MakeFilePathFromNameFileByDate(orderFile);

                File.Delete(filePath);
            }
            else
            {
                WriteAllOrdersToFile(orders, order.OrderDate);
            }

                 
        }

        public bool Update(Order orderToAdd, Order orderToDelete)
        {
            RemoveOrder(orderToDelete);
            AddOrder(orderToAdd);
            return true;
        }

        public void AddOrder(Order order)
        {

            string orderFile = NameFileByDate(order.OrderDate);

            string filePath = MakeFilePathFromNameFileByDate(orderFile);

            if (File.Exists(filePath))
            {
                AppendToFile(filePath, order);
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine(OrderHeader);
                    sw.WriteLine(CreateCommaSeperatedValueFromOrder(order));
                }
            }


        }

        public void WriteAllOrdersToFile(List<Order> orders, DateTime date)
        {
            string orderFile = NameFileByDate(date);

            string filePath = MakeFilePathFromNameFileByDate(orderFile);

            orders.OrderBy(o => o.OrderNumber).ToList();


            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                sw.WriteLine(OrderHeader);
                foreach (var order in orders)
                {
                    sw.WriteLine(CreateCommaSeperatedValueFromOrder(order));
                }
            }
        }

        public int AddOrderNumber(Order order)
        {

            LoadOrders(order.OrderDate);
            //if (_orders.Where(o => o.OrderDate == order.OrderDate).Count() == 0)
            if(_orders == null)
            {
                return 1;
            }
            else
            {
                return _orders.Where(o => o.OrderDate == order.OrderDate).Max(o => o.OrderNumber) + 1;
            }
        }
        /// <summary>
        /// this will make a string to pass into MakeFilePathFromNameFileByDate
        /// it takes the date and string interps the file names needed for the project files
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string NameFileByDate(DateTime date)
        {
            //makes the date brought in turn into mm/dd/yyyy format
            // string stringDate = date.ToShortDateString();

            ////this splits the string above and splits the '/' for an official file date
            //string[] dateParts = stringDate.Split('/');

            //this is the filedate for the txt files to have a name
            //formatting a string with the date passed to make part of the filename.
            string fileDate = ($"Orders_{date.ToString("MMddyyyy")}.txt");

            return fileDate;

        }
        /// <summary>
        /// this will make a new string that will be the file.txt name
        /// when a new date is in effect
        /// </summary>
        /// <param name="filedate"></param>
        /// <returns></returns>
        public string MakeFilePathFromNameFileByDate(string filedate)
        {
            string newfile = $"{fileDirectory}{filedate}";

            return newfile;

        }
        /// <summary>
        /// this method will create a string with the order properties
        /// and make it seperated by commas when needed
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string CreateCommaSeperatedValueFromOrder(Order order)
        {
            string csv = $"{order.OrderNumber},{order.OrderDate.ToShortDateString()},{order.CustomerName}," +
                         $"{order.OrderState.Abbreviation},{order.OrderState.Name},{order.OrderState.TaxRate}," +
                         $"{order.OrderProduct.ProductType},{order.OrderProduct.CostPerSquareFootMaterials},{order.OrderProduct.CostPerSquareFootLabor}," +
                         $"{order.OrderArea},{order.TotalMaterialCost},{order.TotalLaborCost}," +
                         $"{order.TotalTax},{order.OrderTotal}";

            return csv;

        }

        public void AppendToFile(string filename, Order order)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                sw.WriteLine($"{CreateCommaSeperatedValueFromOrder(order)}");
            }
        }



    }
}

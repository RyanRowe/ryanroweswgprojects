﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Repositories.Product
{
    public class FileProductRepository : IProductRepository
    {
        //field for product list
        private List<Models.Models.Product> _products;

        //constructor since these will only be reads 
        public FileProductRepository()
        {
            if (_products == null)
            {
                //this will be made == to the field _products
                List<Models.Models.Product> products = new List<Models.Models.Product>();

                using (
                    //bring up an instance of StreamReader
                    StreamReader sr =
                    //Opening the text file for products given to us, it will always have the same directory
                    //and will only be read
                        File.OpenText(
                            @"C:\_repos\ryan-rowe-individual-work\SGFlooring\FlooringProgram.UI\bin\Debug\DataFiles\Products.txt"))
                {
                    //skips first line since that will be the details of the properties on the file
                    sr.ReadLine();

                    string line = "";

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] inputColumns = line.Split(',');
                        Models.Models.Product product = new Models.Models.Product()
                        {
                            ProductType = inputColumns[0],
                            CostPerSquareFootMaterials = decimal.Parse(inputColumns[1]),
                            CostPerSquareFootLabor = decimal.Parse(inputColumns[2])
                        };

                        products.Add(product);
                    }

                    _products = products;


                }


            }
        }

        public List<Models.Models.Product> GetProducts()
        {
            return _products;
        }

        public Models.Models.Product LoadProductByName(string input)
        {
            return _products.FirstOrDefault(p => p.ProductType.ToUpper() == input.ToUpper());
        }
    }
}

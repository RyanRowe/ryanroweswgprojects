﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Repositories.Mock
{
    public class MockStateRepository : IStateRepository
    {
        private static List<State> _states;

        //constructor since these will never change
        //and always be used
        public MockStateRepository()
        {
            if (_states == null)
            {
                _states = new List<State>()
                {
                    new State()
                    {
                        Abbreviation = "OH",
                        Name = "OHIO",
                        TaxRate = .05M
                    },
                    new State()
                    {
                        Abbreviation = "IN",
                        Name = "INDIANA",
                        TaxRate = .07M
                    },
                    new State()
                    {
                        Abbreviation = "MI",
                        Name = "MICHIGAN",
                        TaxRate = .06M
                    }
                };
            }
        }

        //gets first state that the string to upper
        //FirstOrDefault-Returns the first element of a sequence or a default value if no element is found
        public State LoadState(string input)
        {
            return _states.FirstOrDefault(s => (s.Abbreviation.ToUpper() == input.ToUpper()) || (s.Name.ToUpper() == input.ToUpper()));
        }

        public List<State> LoadStates()
        {
            return _states;
        }
    }
}

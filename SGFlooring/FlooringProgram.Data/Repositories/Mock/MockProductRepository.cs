﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Repositories.Mock
{
    public class MockProductRepository : IProductRepository
    {
        private static List<Models.Models.Product> _products;
        //constructor for mocks to auto pull up
        public MockProductRepository()
        {
            if (_products == null)
            {
                _products = new List<Models.Models.Product>()
                {

                    new Models.Models.Product()

                    {
                        ProductType = "CARPET",
                        CostPerSquareFootLabor = 2.00M,
                        CostPerSquareFootMaterials = 5.00M
                    },

                    new Models.Models.Product()
                    {
                        ProductType = "HARDWOOD",
                        CostPerSquareFootLabor = 3.00M,
                        CostPerSquareFootMaterials = 6.00M
                    },

                    new Models.Models.Product()
                    {
                        ProductType = "CEMENT",
                        CostPerSquareFootLabor = 5.00M,
                        CostPerSquareFootMaterials = 10.00M,
                    }
                };
            }
        }

        public List<Models.Models.Product> GetProducts()
        {
            return _products;
        }

        public Models.Models.Product LoadProductByName(string input)
        {
            //returns the _products list first or default value when the input to upper equals the type
            //FirstOrDefault-Returns the first element of a sequence or a default value if no element is found
            return _products.FirstOrDefault(p => p.ProductType.ToUpper() == input.ToUpper());
        }
    }
}
            











 

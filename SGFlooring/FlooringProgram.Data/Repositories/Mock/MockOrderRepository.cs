﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Models;


namespace FlooringProgram.Data.Repositories.Mock
{
    public class MockOrderRepository : IOrderRepository
    {
        //list used to grab new orders for repos
        private static List<Order> _orders;
        //this is mock information to use for all of the workflows
        //constructor to pull up mocks as soon as Mock is called
        public MockOrderRepository()
        {
            if (_orders == null)
            {
                _orders = new List<Order>()
                {

                 new Order()
                 {
                    OrderNumber = 1,
                    CustomerName = "Ryan",
                    OrderDate = DateTime.Parse("10/11/2016"),
                    OrderArea = 300M,
                    OrderProduct = new Models.Models.Product()
                    {
                       ProductType = "CARPET",
                       CostPerSquareFootLabor = 2.00M,
                       CostPerSquareFootMaterials = 5.00M
                    },
                    OrderState = new State()
                    {
                       Abbreviation = "MI",
                       Name = "MICHIGAN",
                       TaxRate = .06M
                    },
                    TotalMaterialCost = 1500M,
                    TotalLaborCost = 600.00M,
                    TotalTax = 105M,
                    OrderTotal = 2205
                 },
                  new Order()
                 {
                    OrderNumber = 2,
                    CustomerName = "Zach",
                    OrderDate = DateTime.Parse("10/11/2016"),
                    OrderArea = 300M,
                    OrderProduct = new Models.Models.Product()
                    {
                        ProductType = "HARDWOOD",
                        CostPerSquareFootLabor = 3.00M,
                        CostPerSquareFootMaterials = 6.00M
                    },
                    OrderState = new State()
                    {
                        Abbreviation  = "IN",
                        Name = "INDIANA",
                        TaxRate = .07M
                    },
                    TotalMaterialCost = 900M,
                    TotalLaborCost = 1800M,
                    TotalTax = 135M,
                    OrderTotal = 2835
                  },

                  new Order()
                  {
                    OrderNumber = 3,
                    CustomerName = "Jacob",
                    OrderDate = DateTime.Parse("10/10/2016"),
                    OrderArea = 300M,
                    OrderProduct = new Models.Models.Product()
                    {
                        ProductType = "CEMENT",
                        CostPerSquareFootLabor = 5.00M,
                        CostPerSquareFootMaterials = 10.00M,
                    },
                    OrderState = new State()
                    {
                        Abbreviation = "OH",
                        Name = "OHIO",
                        TaxRate = .05M
                    },
                    TotalMaterialCost = 3000M,
                    TotalLaborCost = 1500M,
                    TotalTax = 225M,
                    OrderTotal = 4725M
                   }


                };


            }
        }
        /// <summary>
        /// this method just adds the order to the list we are usinng
        /// </summary>
        /// <param name="order"></param>
        public void AddOrder(Order order)
        {
            _orders.Add(order);
        }
        /// <summary>
        /// this method is called after an order is made,
        /// it assigns an order number from the max order 
        /// number in the current list and adds 1
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public int AddOrderNumber(Order order)
        {
            if (_orders.Where(o => o.OrderDate == order.OrderDate).Count() == 0)
            {
                return 1;
            }
            return _orders.Where(o => o.OrderDate == order.OrderDate).Max(o => o.OrderNumber) + 1;
        }

        /// <summary>
        /// this method will give you whole list of orders 
        /// according to a date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<Order> LoadOrders(DateTime date)
        {
            return _orders.Where(o => o.OrderDate.CompareTo(date) == 0).ToList();
        }
        /// <summary>
        /// this will remove an order in the current list
        /// and bring all other indexes down 1
        /// </summary>
        /// <param name="order"></param>
        public void RemoveOrder(Order order)
        {
            //var result = _orders.FirstOrDefault(o => o.OrderNumber == ordernumber && o.OrderDate == date);
            _orders.Remove(order);
        }

        public bool Update(Order orderToAdd, Order orderToRemove)
        {
            var orderRemove = _orders.FirstOrDefault(o => o.OrderNumber == orderToRemove.OrderNumber);
            _orders.Remove(orderRemove);
           _orders.Add(orderToAdd);
            _orders = _orders.OrderBy(x => x.OrderNumber).ToList();

            return true;
        }

        /// <summary>
        /// After the orders for that date is loaded, I want to pull an order from that list and display it
        /// before prompting to remove or edit, and then in edit let it show old properties while 
        /// they are changing them
        /// </summary>
        /// <param name="orders"></param>
        public Order GetOrderByNumber(List<Order> orders, int ordertopick)
        {
            Order order = new Order();
           
            order = orders[ordertopick - 1];

            return order;
        }

      
    }
}

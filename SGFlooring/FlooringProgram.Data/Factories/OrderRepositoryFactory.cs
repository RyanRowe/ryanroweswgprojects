﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repositories.Mock;
using FlooringProgram.Data.Repositories.Product;

namespace FlooringProgram.Data.Factories
{
    public static class OrderRepositoryFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            //make a app class and this string will be used
            //to change the app setting in the app config file
            //which is references in the data layer and put in the 
            //ui, for this program it is TEST AND PROD to choose the repo
            var mode = ConfigurationManager.AppSettings["Mode"];
            
            switch (mode)
            {
                case "PROD":
                    return new FileOrderRepository();
                case "TEST":
                    return new MockOrderRepository();
                default:                   
                    throw new ArgumentException();
            }

        }
    }
}

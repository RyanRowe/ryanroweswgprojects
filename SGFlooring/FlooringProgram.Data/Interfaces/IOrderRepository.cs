﻿using FlooringProgram.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> LoadOrders(DateTime orderDate);

        void AddOrder(Order order);

        int AddOrderNumber(Order order);

        void RemoveOrder(Order order);

        bool Update(Order orderToAdd, Order orderToRemove);

        Order GetOrderByNumber(List<Order> orders, int choice);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Interfaces
{
    public interface IStateRepository
    {
        State LoadState(string input);

        List<State> LoadStates();
    }
}

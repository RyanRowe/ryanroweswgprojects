﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Data.Interfaces
{
    public interface IProductRepository
    {
       Product LoadProductByName(string input);

        List<Product> GetProducts();
    }
}

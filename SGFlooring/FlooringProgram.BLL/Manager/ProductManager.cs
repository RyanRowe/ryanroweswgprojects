﻿using FlooringProgram.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Responses;

namespace FlooringProgram.BLL
{
    public class ProductManager
    {
        //this will get return a ProductResponse Object
        //and find the object from comparing the string input to Product Type
        //it also has a whole Product Object inside of it as a property
        public ProductResponses GetProductByName(string input)
        {
            ProductResponses response = new ProductResponses();

            IProductRepository productRepository = ProductRepositoryFactory.GetProductRepository();

            var products = productRepository.LoadProductByName(input);

            if (products == null)
            {
                response.Success = false;
                response.Message = $"No Products of the type: {input}";
            }

            else
            {
                response.Success = true;
                response.Products = products;
            }

            return response;

        }

        public ProductListResponse GetProducts()
        {
            ProductListResponse response = new ProductListResponse();

            IProductRepository repos = ProductRepositoryFactory.GetProductRepository();

            List<Product> productlist = repos.GetProducts();

            if (productlist == null)
            {
                response.success = false;
                response.Message = "No Products Loaded So Sorry";
            }

            else
            {
                response.success = true;
                response.ProductList = productlist;
            }

            return response;
        }
    }
}

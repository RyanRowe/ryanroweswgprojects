﻿using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Repositories.Product;
using FlooringProgram.Models.Models;

namespace FlooringProgram.BLL
{
    public class StateManager
    {

        /// <summary>
        /// this will return a state response and use
        /// the data layer method to load a state by 
        /// the string
        /// user input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public StateResponses GetStateByName(string input)
        {
            StateResponses response = new StateResponses();

            IStateRepository stateRepository = StateRepositoryFactory.GetStateRepository();

            var state = stateRepository.LoadState(input);

            if (state == null)
            {
                response.Success = false;
                response.Message = $"No State: {input}";
            }

            else
            {
                response.Success = true;
                response.States = state;
            }

            return response;

        }

        public StateListResponse GetStates()
        {
            StateListResponse response = new StateListResponse();

            IStateRepository stateRepository = StateRepositoryFactory.GetStateRepository();

            List<State> StateList = stateRepository.LoadStates();

            if (StateList == null)
            {
                response.Success = false;
                response.Message = "No States Loaded So Sorry";
            }

            else
            {
                response.Success = true;
                response.StateList = StateList;
            }

            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Factories;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.Repositories.Mock;
using FlooringProgram.Models.Models;
using FlooringProgram.Models.Responses;

namespace FlooringProgram.BLL
{
    public class OrderManager
    {
        /// <summary>
        /// this gets you a list of orders according to the date
        /// of the user input
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public OrderResponses GetOrdersByDate(DateTime date)
        {
            OrderResponses response = new OrderResponses();

            IOrderRepository orderRepository = OrderRepositoryFactory.GetOrderRepository();

            var orders = orderRepository.LoadOrders(date);

            if (orders == null || orders.Count == 0)
            {
                response.Success = false;
                response.Message = $"No orders found with that date: {date}";
                
            }

            else
            {
                response.Success = true;
                response.FilteredOrders = orders;
            }

            return response;


        }

        /// <summary>
        /// I want to bring in the the list of orders by date and 
        /// let them choose a number to pull and individual object
        /// </summary>
        /// <param name="date"></param>
        /// <param name="choice"></param>
        /// <returns></returns>
        public OrderResponse GetOrder(List<Order> orders, int choice)
        {
            OrderResponse response = new OrderResponse();

            IOrderRepository repo = OrderRepositoryFactory.GetOrderRepository();


            var order = repo.GetOrderByNumber(orders, choice);

            if (order == null)
            {
                response.Success = false;
                response.Message = "No orders found!!!!";
            }

            else
            {
                response.Success = true;
                response.FilteredOrder = order;
            }

            return response;



        }

        /// <summary>
        /// this calculates the order passed in the other properties 
        /// that rely on the user input to calculate
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public Order CalculateAddOrder(Order order)
        {
       
            order.TotalLaborCost = order.OrderArea * order.OrderProduct.CostPerSquareFootLabor;
            order.TotalMaterialCost = order.OrderArea * order.OrderProduct.CostPerSquareFootMaterials;
            order.TotalTax = (order.TotalLaborCost + order.TotalMaterialCost) * (order.OrderState.TaxRate);
            order.OrderTotal = order.TotalLaborCost + order.TotalMaterialCost + order.TotalTax;
            return order;
        }

        /// <summary>
        /// after user decides to confirm add this method will
        /// add the order object to the current repos and use 
        /// the repos method to add to it
        /// </summary>
        /// <param name="order"></param>
        public void AddConfirmedOrder(Order order)
        {
            IOrderRepository repo = OrderRepositoryFactory.GetOrderRepository();
            repo.AddOrder(order);
        }

        /// <summary>
        /// this method calls the repo and uses the add order
        /// method in the repo to gives the passed in order 
        /// and order number of max order number plus one
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public Order AddOrderNumber(Order order)
        {
            IOrderRepository repo = OrderRepositoryFactory.GetOrderRepository();
            order.OrderNumber = repo.AddOrderNumber(order);
            return order;
        }

        /// <summary>
        /// this method will call the repos and 
        /// use the repos method to remove order 
        /// from list
        /// </summary>
        /// <param name="order"></param>
        public void RemoveOrder(Order order)
        {
            IOrderRepository repo = OrderRepositoryFactory.GetOrderRepository();
            repo.RemoveOrder(order);
        }

        /// <summary>
        /// this method will call the repo bring in an order
        /// and will use the update method in the repo that will remove 
        /// the old object and add the new one
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public OrderResponse UpdateOrder(Order orderToAdd, Order orderToDelete)
        {
            OrderResponse response = new OrderResponse();

            var repo = OrderRepositoryFactory.GetOrderRepository();

            if (repo.Update(orderToAdd, orderToDelete))
            {
                response.Success = true;
                response.FilteredOrder = orderToAdd;
            }

            else
            {
                response.Success = false;
                response.Message = "Failed to Create new account";

            }

            return response;
        }


    }
}

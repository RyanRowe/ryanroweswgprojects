﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Models
{
    public class Product

    {
        //Products are an object that is a property of Order.cs

        public string ProductType { get; set; }
        public decimal CostPerSquareFootMaterials { get; set; }
        public decimal CostPerSquareFootLabor { get; set; }


    }
}

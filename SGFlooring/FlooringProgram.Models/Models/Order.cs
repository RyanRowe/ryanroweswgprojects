﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Models
{
    public class Order
    {
        // * = Properties to prompt user for everything else will be calculated
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; } //*
        public string CustomerName { get; set; } //*
        public State OrderState { get; set; }         
        public Product OrderProduct { get; set; } //*       
        public decimal OrderArea { get; set; }
        public decimal TotalMaterialCost { get; set; }
        public decimal TotalLaborCost { get; set; }
        public decimal TotalTax { get; set; }
        public decimal OrderTotal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models.Models;

namespace FlooringProgram.Models.Responses
{
    public class ProductListResponse
    {
        public bool success { get; set; }
        public string Message { get; set; }
        public List<Product> ProductList{ get; set;}    
    }
}

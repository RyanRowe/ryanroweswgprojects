﻿using FlooringProgram.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models.Responses
{
    public class ProductResponses
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Product Products { get; set; }
    }
}

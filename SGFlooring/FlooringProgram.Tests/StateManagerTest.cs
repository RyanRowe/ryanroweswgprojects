﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{   
    [TestFixture]
    class StateManagerTest
    {
        [Test]
        public void GetStatesTest()
        {
            StateManager manager = new StateManager();

            var response = manager.GetStates();

            Assert.IsTrue(response.Success);

            foreach (var state in response.StateList)
            {
                Console.WriteLine($"{state.Name}, {state.Abbreviation}, {state.TaxRate}");
            }

        }
    }
}

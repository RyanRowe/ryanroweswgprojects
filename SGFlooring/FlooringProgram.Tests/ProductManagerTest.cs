﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class ProductManagerTest
    {
        [Test]
        public void GetProductsTest()
        {
            ProductManager manager = new ProductManager();

            var response = manager.GetProducts();

            Assert.IsTrue(response.success);

            foreach (var product in response.ProductList)
            {
                Console.WriteLine($"{product.CostPerSquareFootLabor}, {product.CostPerSquareFootMaterials}, {product.ProductType}");
            }
            
        }
    }
}

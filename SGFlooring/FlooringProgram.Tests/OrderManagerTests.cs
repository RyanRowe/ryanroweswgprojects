﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.BLL;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    [TestFixture]
    class OrderManagerTests
    {
        [Test]
        public void GetOrdersByDateTest()
        {
            DateTime date = DateTime.Parse("10/11/16");

            OrderManager ordermanager = new OrderManager();
            var response = ordermanager.GetOrdersByDate(date);
            Assert.IsTrue(response.Success);

            foreach (var order in response.FilteredOrders)
            {
                Console.WriteLine($"{order.OrderNumber}, {order.CustomerName}, {order.OrderArea}");
            }
        }

        [Test]
        public void GetOrderTest()
        {
            OrderManager manager = new OrderManager();

            int choice = 1;
            var date = DateTime.Parse("10/11/16");
            var orders = manager.GetOrdersByDate(date);

            Assert.IsTrue(orders.Success);

            var order = manager.GetOrder(orders.FilteredOrders, choice);

            Assert.IsTrue(order.Success);

            Console.WriteLine($"{order.FilteredOrder.OrderNumber}, {order.FilteredOrder.CustomerName}, {order.FilteredOrder.OrderProduct.ProductType}, {order.FilteredOrder.OrderDate.ToShortDateString()}");

        }



    }
}

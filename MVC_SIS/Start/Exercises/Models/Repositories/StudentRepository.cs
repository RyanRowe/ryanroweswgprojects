﻿using Exercises.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exercises.Models.Repositories
{
    public class StudentRepository
    {
        private static List<Student> _students;

        static StudentRepository()
        {
            // sample data
            _students = new List<Student>
            {
                new Student {
                    StudentId=1,
                    FirstName="John",
                    LastName="Doe",
                    StudentEmail = "JohnDoe@RyanSis.edu",
                    SecondaryEmail="ChristianSucksAtLeague@gmail.com",
                    Phone = "(111) 867 5309",
                    GPA=2.5M,
                    Major=new Major { MajorId=1,  MajorName="Art" },
                    Address=new Address {                      
                        Street1="123 Main St",
                        City="Akron",
                        State=new State {
                            StateId = 1,
                            StateAbbreviation="OH",
                            StateName="Ohio"},
                        PostalCode="44311"
                    },
                    Courses=new List<Course>
                    {
                        new Course { CourseId=1, CourseName="Art History" },
                        new Course { CourseId=8, CourseName="Photography" }
                    }
                },
                new Student {
                    StudentId=2,
                    FirstName="Jane",
                    LastName="Wicks",
                    StudentEmail = "JaneWicks@RyanSis.edu",
                    SecondaryEmail="RyanisMLG@gmail.com",
                    Phone = "(111) 867 5308",
                    GPA=3.5M,
                    Major=new Major { MajorId=2,  MajorName="Business" },
                    Address=new Address {
                        Street1="422 Oak St",
                        City="Mineapolis",
                        State=new State {
                             StateId = 2,
                            StateAbbreviation="MN",
                            StateName="Minnesota"},
                        PostalCode="55401"
                    },
                    Courses=new List<Course>
                    {
                        new Course { CourseId=2, CourseName="Accounting 101" },
                        new Course { CourseId=4, CourseName="Business Law" },
                        new Course { CourseId=6, CourseName="Economics 101" },
                    }
                },
                new Student {
                        StudentId=3,
                    FirstName="Megan",
                    LastName="Smith",
                    StudentEmail = "MeganSmith@RyanSis.edu",
                    SecondaryEmail="ThatsOneWayToDoIt@gmail.com",
                    Phone = "(111) 867 4309",
                    GPA=3.5M,
                    Major=new Major { MajorId=1,  MajorName="Art" },
                    Address=new Address {
                        Street1="345 Pain St",
                        City="Akron",
                        State=new State {
                             StateId = 3,
                            StateAbbreviation="OH",
                            StateName="Ohio"},
                        PostalCode="44312"
                    },
                    Courses=new List<Course>
                    {
                        new Course { CourseId=1, CourseName="Art History" },
                        new Course { CourseId=8, CourseName="Photography" }
                    }                
                },
            };
        }

        public static IEnumerable<Student> GetAll()
        {
            return _students;
        }

        public static Student Get(int studentId)
        {
            return _students.FirstOrDefault(s => s.StudentId == studentId);
        }

        public static void Add(Student student)
        {
            if (_students.Count == 0)
            {
                student.StudentId = 1;
            }
            else
            {
                student.StudentId = _students.Max(s => s.StudentId) + 1;
            }
            _students.Add(student);

            Random random = new Random();

            student.StudentEmail = $"{student.FirstName}{student.LastName}{random.Next(1,300)}@RyanSis.Edu";


        }

        /// <summary>
        /// this method brings in the new properties from the user input 
        /// and makes a new instance of a the person selected from 
        /// the old list by student id and changes all of the
        /// properties to the new inputs
        /// </summary>
        /// <param name="student"></param>
        public static void Edit(Student student)
        {
            var selectedStudent = _students.First(s => s.StudentId == student.StudentId);

            Random random = new Random();
            
            selectedStudent.FirstName = student.FirstName;
            selectedStudent.LastName = student.LastName;
            selectedStudent.SecondaryEmail = student.SecondaryEmail;
            selectedStudent.Phone = student.Phone;
            selectedStudent.GPA = student.GPA;
            selectedStudent.Major = student.Major;
            selectedStudent.Courses = student.Courses;
            selectedStudent.Address = student.Address;
            selectedStudent.StudentEmail = $"{student.FirstName}{student.LastName}{random.Next(1,300)}@RyanSis.edu";

        }
        
        /// <summary>
        /// finds the student by the student id
        /// integer prop and removes all students 
        /// associated with that number
        /// </summary>
        /// <param name="studentId"></param>
        public static void Delete(int studentId)
        {
            _students.RemoveAll(s => s.StudentId == studentId);
        }


        /// <summary>
        /// Not sure I will ever need this method, but could used to path address from 
        /// one student to another?
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="address"></param>
        public static void SaveAddress(int studentId, Address address)
        {
            var selectedStudent = _students.First(s => s.StudentId == studentId);
            selectedStudent.Address = address;
        }
    }
}
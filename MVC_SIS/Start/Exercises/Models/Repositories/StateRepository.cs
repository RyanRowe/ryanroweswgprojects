﻿using Exercises.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exercises.Models.Repositories
{
    public class StateRepository
    {
        private static List<State> _states;

        static StateRepository()
        {
            // sample data
            _states = new List<State>
            {
                new State {StateId = 1, StateAbbreviation="KY", StateName="Kentucky" },
                new State {StateId = 2, StateAbbreviation="MN", StateName="Minnesota" },
                new State {StateId = 3, StateAbbreviation="OH", StateName="Ohio" },
            };
        }

        public static IEnumerable<State> GetAll()
        {
            return _states;
        }

        public static State Get(int stateId)
        {
            return _states.FirstOrDefault(s => s.StateId == stateId);
        }

        public static void Add(string statename, string stateabb)
        {
            State state = new State();
            state.StateName = statename;
            state.StateAbbreviation = stateabb;
            if (_states.Count == 0)
            {
                state.StateId = 1;
            }
            else
            {
                state.StateId= _states.Max(s => s.StateId) + 1;
            }         
            _states.Add(state);
        }

        public static void Edit(State state)
        {
            var selectedState = _states.FirstOrDefault(s => s.StateId == state.StateId);

            selectedState.StateName = state.StateName;
            selectedState.StateAbbreviation = state.StateAbbreviation;
        }

        public static void Delete(int stateid)
        {
            _states.RemoveAll(s => s.StateId == stateid);
        }
    }
}
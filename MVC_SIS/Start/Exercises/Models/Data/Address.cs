﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercises.Models.Data
{
    public class Address
    {   
        [Required(ErrorMessage = "Please Enter a Street Name")]
        public string Street1 { get; set; }  
        [Required(ErrorMessage = "Please enter a City")]     
        public string City { get; set; }               
        public State State { get; set; }
        [Required(ErrorMessage = "Please Enter a Zip")]
        public string PostalCode { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Exercises.Models.Data
{
    public class Student
    {

        public int StudentId { get; set; }
        [Required(ErrorMessage = "Please Enter a First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter a Last Name")]
        public string LastName { get; set; }
        public string StudentEmail { get; set; }
        [Required(ErrorMessage = "Please Enter an Email")]
        [EmailAddress]
        public string SecondaryEmail { get; set; }
        [Required(ErrorMessage = "Please Enter a Phone Number")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Please Enter a GPA")]
        [Range(0,4)]
        public decimal? GPA { get; set; }
        public Address Address { get; set; }
        public Major Major { get; set; }
        public List<Course> Courses { get; set; }
    }
}
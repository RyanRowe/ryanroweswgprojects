﻿$(document)
    .ready(function () {
        $('#studentForm')
            .validate({
                rules: {
                    "Student.FirstName": {
                        required: true
                    },
                    "Student.LastName": {
                        required: true

                    },
                    "Student.SecondaryEmail": {
                        required: true,
                        email: true
                    },
                    "Student.Phone": {
                        required: true,
                        phoneUS: true
                    },
                    "Student.GPA": {
                        required: true,
                        range: [0, 4]
                    },
                    "Student.Address.Street1": {
                        required: true
                    },
                    "Student.Address.City": {
                        required: true
                    },
                    "Student.Address.PostalCode": {
                        required: true,
                        rangelength: [5, 5]
                    },
                    "Student.Major.MajorId": {
                        required: true
                    },
                    "Student.Address.State.StateId": {
                        required: true
                    }

                },
                messages: {
                    "Student.FirstName": {
                        required: "Please enter a First Name"
                    },
                    "Student.LastName": {
                        required: "Please enter a Last Name"
                    },
                    "Student.SecondaryEmail": {
                        required: "Please enter an Email",
                        email: "NOT THE RIGHT FORMAT FOR EMAIL!"
                    },
                    "Student.Phone": {
                        required: "Please enter a Phone Number",
                        phoneUS: "Not a Valid Phone Number"
                    },
                    "Student.GPA": {
                        required: "Please enter a GPA",
                        range: "Please enter a GPA in range!!"
                    },
                    "Student.Address.Street1": {
                        required: "Please enter a Street Address"
                    },
                    "Student.Address.City": {
                        required: "Please enter a City"
                    },
                    "Student.Address.PostalCode": {
                        required: "Please Enter a Zip",
                        rangelength: "5 Digit Zip Code Please"
                    },
                    "Student.Major.MajorId": {
                        required: "Please Pick a Major"
                    },
                    "Student.Address.State.StateId": {
                        required: "Please Pick a State"
                    }
                }

            });
    });
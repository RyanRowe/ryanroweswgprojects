﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Factory;
using CarDealership.Data.Interface;
using CarDealership.Models;

namespace CarDealership.BLL
{
    public class OperationsManager
    {
        private ICarRepo _CarRepo;

        public OperationsManager( )
        {
            _CarRepo = CarRepoFactory.CreatCarRepo( );
        }

        public Response AddCarResponse( Vehicle car )
        {
            bool isValid = _CarRepo.Add( car );
            return Helper.SetIfElse( isValid, "You did something wrong when trying to add that vehicle" );
        }



        public Response DeleteVehicle(int VehicleId)

        {
            bool isValid = _CarRepo.Remove(VehicleId);
            return Helper.SetIfElse( isValid, "You did something wrong when trying to Remove that vehicle" );
        }

        public List<Vehicle> ListCars( )
        {
            return _CarRepo.GetAll( );
        }

        public Vehicle GetVehicleById( int VehicleId)
        {
            return _CarRepo.GetById(VehicleId);
        }

        public Response EditVehicle(Vehicle car)
        {
            bool isValid = _CarRepo.Edit(car);
            return Helper.SetIfElse(isValid, "You did something wrong when trying to Edit that vehicle");

        }
    }
}
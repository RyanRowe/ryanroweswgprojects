﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.BLL
{
    public class Helper
    {
        public static Response SetIfElse( bool isValid, string message )
        {
            Response response = new Response( );

            if(isValid == true)
            {
                response.Success = true;
            }
            else
            {
                response.Success = false;
                response.Message = message;
            }
            return response;
        }
    }
}
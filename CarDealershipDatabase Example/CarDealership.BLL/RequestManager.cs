﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Factory;
using CarDealership.Data.Interface;
using CarDealership.Models;

namespace CarDealership.BLL
{
    public class RequestManager
    {
        private IRequestRepo _requestRepo;

        public RequestManager()
        {
            _requestRepo = RequestFactory.CreateRequestRepo();
        }

        public Response AddRequest(Request request)
        {
            Request newRequest = request;
            newRequest.VehicleName = GetVehicleName(newRequest);
            var totalRequests = GetListOfRequests();
            newRequest.RequestID = totalRequests.Count + 1;
            
            bool validResponse = _requestRepo.AddRequest(newRequest);
        
           
            return Helper.SetIfElse(validResponse, "Add request was invalid, please try again");
        }

       

        private string GetVehicleName(Request request)
        {
            ICarRepo carRepo = CarRepoFactory.CreatCarRepo();

            var vehicle = carRepo.GetById(request.VehicleId);

            string vehicleName = $"{vehicle.Year} {vehicle.Make} {vehicle.Model}";

            return vehicleName;
        }

        public Response DeleteRequest(int RequestID)
        {
            bool validResponse = _requestRepo.DeleteRequest(RequestID);
            return Helper.SetIfElse(validResponse, "Delete request was invalid, please try again");
        }

        public Response EditResponse(Request request)
        {
            bool validResponse = _requestRepo.EditRequest(request);
            return Helper.SetIfElse(validResponse, "Edit request was invalid, please try again");
        }

        public List<Request> GetListOfRequests()
        {
            return _requestRepo.GetAllRequests();
        }

        public Request GetRequestById(int RequestID)
        {
            return _requestRepo.GetByRequestId(RequestID);
        }
        
    }
}

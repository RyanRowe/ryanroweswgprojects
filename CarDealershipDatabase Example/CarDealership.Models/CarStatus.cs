﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models
{
    public enum CarStatus
    {
        Used,
        New,
        CertifiedUsed,
        Sold,
        NotAvalible
    
    }
}

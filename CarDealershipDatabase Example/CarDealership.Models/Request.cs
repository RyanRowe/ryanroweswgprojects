﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CarDealership.Models
{
   public  class Request
    {

        public int RequestID { get; set; }
        [Required(ErrorMessage = "Customer name is required")]
        public int VehicleId { get; set; }
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "E-mail is required")]
        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$", ErrorMessage = "Correct format is: person@example.com")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$", ErrorMessage = "Correct format is 111-111-1111")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Best time to call is required")]
        public BestTimeToCall TimeToCall { get; set; }
        [Required(ErrorMessage ="Contact method is required")]
        public string ContactMethod { get; set; }
        [Required(ErrorMessage = "Vehicle name is required")]
        public string VehicleName { get; set; }
        public RequestStatus RequestStatus { get; set; }

        public string TimeFrame { get; set; }

        public RequestStatus StatusOfRequest { get; set; }

        //Constructor to set request intially to new 
        public Request()
        {
            StatusOfRequest = RequestStatus.New;
        }

    }
}

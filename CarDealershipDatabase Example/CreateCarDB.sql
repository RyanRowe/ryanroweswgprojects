USE [master]
GO
/****** Object:  Database [CarDealership]    Script Date: 11/12/2016 10:39:59 PM ******/
CREATE DATABASE [CarDealership]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CarDealership', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\CarDealership.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CarDealership_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\CarDealership_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CarDealership] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CarDealership].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CarDealership] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CarDealership] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CarDealership] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CarDealership] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CarDealership] SET ARITHABORT OFF 
GO
ALTER DATABASE [CarDealership] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CarDealership] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CarDealership] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CarDealership] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CarDealership] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CarDealership] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CarDealership] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CarDealership] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CarDealership] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CarDealership] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CarDealership] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CarDealership] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CarDealership] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CarDealership] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CarDealership] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CarDealership] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CarDealership] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CarDealership] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CarDealership] SET  MULTI_USER 
GO
ALTER DATABASE [CarDealership] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CarDealership] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CarDealership] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CarDealership] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CarDealership] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CarDealership]
GO
/****** Object:  Table [dbo].[Requests]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Requests](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleId] [int] NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[TimeToCall] [int] NULL,
	[ContactMethod] [varchar](50) NULL,
	[VehicleName] [varchar](50) NULL,
	[TimeFrame] [varchar](50) NULL,
	[StatusOfRequest] [int] NULL,
 CONSTRAINT [PK_Requests] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RequestsForCars]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestsForCars](
	[RequestID] [int] NOT NULL,
	[VehicleId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vehicles]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vehicles](
	[VehicleId] [int] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[Mileage] [int] NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_Vehicles] PRIMARY KEY CLUSTERED 
(
	[VehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Requests] ON 

INSERT [dbo].[Requests] ([RequestID], [VehicleId], [CustomerName], [Phone], [Email], [TimeToCall], [ContactMethod], [VehicleName], [TimeFrame], [StatusOfRequest]) VALUES (4, 1, N'Benjamin Kraus', N'2168486664', N'benk622@gmail.com', 1, N'2168486664', N'2015 Honda Civic', N'two weeks', 1)
INSERT [dbo].[Requests] ([RequestID], [VehicleId], [CustomerName], [Phone], [Email], [TimeToCall], [ContactMethod], [VehicleName], [TimeFrame], [StatusOfRequest]) VALUES (5, 1, N'Test', N'2168486664', N'benk622@gmail.com', 1, N'2168486664', N'2015 Honda Civic', N'one week', 0)
SET IDENTITY_INSERT [dbo].[Requests] OFF
SET IDENTITY_INSERT [dbo].[Vehicles] ON 

INSERT [dbo].[Vehicles] ([VehicleId], [Make], [Model], [Year], [Mileage], [Price], [Description], [Title], [status]) VALUES (1, N'Honda', N'Civic', 2015, 100001111, CAST(150001 AS Decimal(18, 0)), N'This is a great car', N'Great Car!', 0)
INSERT [dbo].[Vehicles] ([VehicleId], [Make], [Model], [Year], [Mileage], [Price], [Description], [Title], [status]) VALUES (9, N'Ford', N'Mustang', 2015, 100001, CAST(20000 AS Decimal(18, 0)), N'Great car', N'car looking for home', 1)
SET IDENTITY_INSERT [dbo].[Vehicles] OFF
ALTER TABLE [dbo].[Requests]  WITH CHECK ADD  CONSTRAINT [FK_Requests_Vehicles] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicles] ([VehicleId])
GO
ALTER TABLE [dbo].[Requests] CHECK CONSTRAINT [FK_Requests_Vehicles]
GO
ALTER TABLE [dbo].[RequestsForCars]  WITH CHECK ADD  CONSTRAINT [FK_RequestsForCars_Requests] FOREIGN KEY([RequestID])
REFERENCES [dbo].[Requests] ([RequestID])
GO
ALTER TABLE [dbo].[RequestsForCars] CHECK CONSTRAINT [FK_RequestsForCars_Requests]
GO
ALTER TABLE [dbo].[RequestsForCars]  WITH CHECK ADD  CONSTRAINT [FK_RequestsForCars_Vehicles] FOREIGN KEY([VehicleId])
REFERENCES [dbo].[Vehicles] ([VehicleId])
GO
ALTER TABLE [dbo].[RequestsForCars] CHECK CONSTRAINT [FK_RequestsForCars_Vehicles]
GO
/****** Object:  StoredProcedure [dbo].[spDeleteRequest]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteRequest](
	@RequestID int
) AS
BEGIN
	DELETE Requests
	WHERE RequestID = @RequestID;
END
GO
/****** Object:  StoredProcedure [dbo].[spRequestAdd]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRequestAdd](
	@RequestId int,
	@VehicleId int,
	@CustomerName varchar(50),
	@Phone varchar(50),
	@Email varchar(50),
	@TimeToCall int,
	@ContactMethod varchar(50),
	@VehicleName varchar (50),
	@TimeFrame   varchar (50),
	@StatusOfRequest int 
) AS
BEGIN
	INSERT INTO Requests (VehicleId, CustomerName, Phone, Email, TimeToCall, ContactMethod, VehicleName, TimeFrame, StatusOfRequest)
	VALUES (@VehicleId, @CustomerName, @Phone, @Email, @TimeToCall, @ContactMethod, @VehicleName, @TimeFrame, @StatusOfRequest);

	SET @RequestId = SCOPE_IDENTITY();
END
GO
/****** Object:  StoredProcedure [dbo].[spRequestEdit]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRequestEdit] (
	@RequestId int,
	@VehicleId int,
	@CustomerName varchar(50),
	@Phone varchar(50),
	@Email varchar(50),
	@TimeToCall int,
	@ContactMethod varchar(50),
	@VehicleName varchar (50),
	@TimeFrame   varchar (50),
	@StatusOfRequest int
) AS 
BEGIN 
	UPDATE Requests
	SET VehicleId = @VehicleId,
		CustomerName = @CustomerName,
		Phone = @Phone,
		Email = @Email,
		TimeToCall = @TimeToCall,
		ContactMethod = @ContactMethod,
		VehicleName = @VehicleName,
		TimeFrame = @TimeFrame,
		StatusOfRequest = @StatusOfRequest
	WHERE RequestID = @RequestId
END
GO
/****** Object:  StoredProcedure [dbo].[spVehicleDelete]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Delete a vehicle
Create Procedure [dbo].[spVehicleDelete](
	@VehicleId int
) AS
BEGIN
	Delete Vehicles
	Where VehicleId = @VehicleId;
End
GO
/****** Object:  StoredProcedure [dbo].[spVehicleEdit]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Edit a vehicle
Create Procedure [dbo].[spVehicleEdit](
	@Make varchar(50),
	@Model varchar(50),
	@Year int,
	@Mileage int,
	@Price decimal(18,0),
	@Description varchar(100),
	@Title varchar(50),
	@status int,
	@VehicleId int
) AS
BEGIN
	UPDATE Vehicles
	SET Make = @Make,
		Model = @Model,
		Year = @Year,
		Mileage = @Mileage,
		Price = @Price,
		Description = @Description,
		Title = @Title,
		status = @status
	Where VehicleId = @VehicleId
End
GO
/****** Object:  StoredProcedure [dbo].[VehicleAdd]    Script Date: 11/12/2016 10:40:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VehicleAdd](
	@Make varchar(50),
	@Model varchar(50),
	@Year int,
	@Mileage int,
	@Price decimal(18,0),
	@Description varchar(100),
	@Title varchar(50),
	@status int,
	@VehicleId int 
)AS
BEGIN
	INSERT INTO Vehicles (Make, Model, Year, Mileage, Price, Description, Title, status)
	Values (@Make, @Model, @Year, @Mileage, @Price, @Description, @Title, @status)

	Set @VehicleId = SCOPE_IDENTITY();
End

GO
USE [master]
GO
ALTER DATABASE [CarDealership] SET  READ_WRITE 
GO

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarDealership.Models;
using CarDealership.BLL;


namespace CarDealership.Web.Controllers
{
    public class AdminController : Controller
    {
        RequestManager requestOps = new RequestManager();
        

        
        // GET: Admin
        public ActionResult ListOfRequests()
        {
            var request = requestOps.GetListOfRequests();
            return View(request);
        }

        public ActionResult RequestDetail(int RequestID)
        {
            var request = requestOps.GetRequestById(RequestID);
            return View(request);

        }
        [HttpGet]
        public ActionResult AddRequest(int VehicleId)
        {

            var request= new Request();
            request.VehicleId = VehicleId;
      

            return View(request);
        }

        [HttpPost]
        public ActionResult AddRequest(Request request)
        {
            var ops = new RequestManager();
            ops.AddRequest(request);

            return RedirectToAction("ListOfRequests");


        }
        [HttpGet]
        public ActionResult EditRequest(int RequestID)
        {

            var ops = new RequestManager();
            var request=ops.GetRequestById(RequestID);
            return View(request);
        }

        [HttpPost]
        public ActionResult EditRequest(Request request)
        {
            var ops = new RequestManager();
            ops.EditResponse(request);

            return RedirectToAction("ListOfRequests");


        }
        [HttpGet]
        public ActionResult RemoveRequest(int RequestID)
        {

            var ops = new RequestManager();
            var request = ops.GetRequestById(RequestID);
            return View(request);
        }

        [HttpPost]
        public ActionResult RemoveRequest(Request request)
        {
            var ops = new RequestManager();
            ops.DeleteRequest(request.RequestID);

            return RedirectToAction("ListOfRequests");


        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;

namespace CarDealership.Web.Controllers
{
    public class CarController : Controller
    {
        OperationsManager ops = new OperationsManager();

        [HttpGet]
        public ActionResult CarDetail(int VehicleId)
        {

            var CarToView = ops.GetVehicleById(VehicleId);
            return View(CarToView);
        }

        [HttpGet]
        public ActionResult AddCar()
        {
            var vehicle = new Vehicle();
            return View(vehicle);
        }

        [HttpPost]
        public ActionResult AddCar(Vehicle car)
        {
            var response = ops.AddCarResponse(car);

            return RedirectToAction("Home");
        }


        [HttpGet]
        public ActionResult RemoveCar(int vehicleID)
        {
            var vehicle = ops.GetVehicleById(vehicleID);

            return View(vehicle);
        }

        [HttpPost]
        public ActionResult RemoveCar(Vehicle vehicle)
        {
            var response = ops.DeleteVehicle(vehicle.VehicleId);

            return RedirectToAction("Home");
        }


        [HttpGet]
        public ActionResult EditCar(int VehicleId)
        {
            
            var CarToEdit= ops.GetVehicleById(VehicleId);
            return View(CarToEdit);
        }

        [HttpPost]
        public ActionResult EditCar(Vehicle car)
        {  
            ops.EditVehicle(car);
            return RedirectToAction("Home");
        }
        public ActionResult RequestInfo()
        {
            return View();
        }

  

        public ActionResult Home()
        {
            return View();
        }

        public ActionResult CarPortal()
        {
            return View();
        }

        public ActionResult CarPortalCarDetails(int VehicleId)
        {
            var CarToView = ops.GetVehicleById(VehicleId);
            return View(CarToView);
        }
    }
}

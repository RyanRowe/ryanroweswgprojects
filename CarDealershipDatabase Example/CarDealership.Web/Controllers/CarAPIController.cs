﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarDealership.BLL;
using CarDealership.Models;

namespace CarDealership.Web.Controllers
{
    public class CarAPIController : ApiController
    {
        public List<Vehicle> Get()
        {

            var ops = new OperationsManager();
            return ops.ListCars();
        }

        public Vehicle Get(int vehicleID)
        {
            var ops = new OperationsManager();
            return ops.GetVehicleById(vehicleID);
        }

        public HttpResponseMessage Post(Vehicle car)
        {
            var ops = new OperationsManager();
            ops.AddCarResponse(car);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

    }
}

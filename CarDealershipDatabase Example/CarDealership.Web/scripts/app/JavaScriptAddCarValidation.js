﻿$(document)
    .ready(function () {
        $('#carForm')
            .validate({
                rules: {
                    Year: {
                        required: true
                    },
                    Make: {
                        required: true    
                    },
                    Model: {
                        required: true
                    },
                    Mileage: {
                        required: true
                    },
                    Price: {
                        required: true
                    },
                    Description: {
                        required: true
                    },
                    Title: {
                    required: true
                    },
                    status: {
                        required: true
                    }

                },
                messages: {
                    Year: {
                        required: "Enter the vehicle year"
                    },
                    Make: {
                        required: "Enter the vehicle Make"
                    },
                    Model: {
                        required: "Enter the vehicle Model"
                    },
                    Mileage: {
                        required: "Enter the mileage"
                    },
                    Price: {
                        required: "Enter the price"
                    },
                    Description: {
                        required: "Enter the vehicle description"
                    },
                    Title: {
                        required: "Enter a Title"
                    },
                    status: {
                        required: "Select a status"
                    }
                }
            });
    });
﻿var myApp = angular.module('carApp', ['ngRoute']);

myApp.factory('carFactory',
    function ($http) {
        // create a new object
        var webAPIProvider = {};

        // url of our WebAPI Controller
        var url = '/api/CarAPI/';

        // add a method to get all the cars
        webAPIProvider.getCars = function () {
            return $http.get(url);
        };

        // add a method to add a car
        webAPIProvider.saveCar = function (vehicle) {
            return $http.post(url, vehicle);
        };

        //Need method for getting specific car
        webAPIProvider.getCar = function (vehicleId) {
            return $http.post(url, vehicleId);
        };
        // return our wrapped API object
        return webAPIProvider;
    });

// this is configuring the routes that we wish to use
// in our application. 
myApp.config(function ($routeProvider) {
    // when we get to the route specified set these options.
    $routeProvider.when('/Home',
        {
            controller: 'CarController',
            templateUrl: '/AngularViews/Cars.html'
        })
        .when('/AddCar',
        {
            controller: 'AddCarController',
            templateUrl: '/AngularViews/AddCar.html'
        })
        .when('/ViewCar',
        {
            controller: 'ViewCarController',
            templateUrl: '/AngularViews/ViewCar.html'
            
        })
        // if no route is recognized do this
        .otherwise({ redirectTo: '/Home' });
});

// controller for the get list.
myApp.controller('CarController',
    function ($scope, carFactory) {
        // call the get friends method as defined above. 
        carFactory.getCars()
            .success(function (data) {
                // if the call succeeds set the scope 
                // variable called car.
                $scope.Vehicle = data;
            })
            .error(function (data, status) {
                alert('oh crap! status: ' + status);
            });
    });

myApp.controller('ViewCarController',
    function ($scope, carFactory) {
        // call the get cars method as defined above. 
        carFactory.getCar()
            .success(function (data) {
                // if the call succeeds set the scope 
                // variable called car.
                $scope.Vehicle = data;
            })
            .error(function (data, status) {
                alert('oh crap! status: ' + status);
            });
    });

// controller for the add friend
myApp.controller('AddCarController',
    function ($scope, $location, carFactory) {
        // model bind a new object
        $scope.Vehicle = {};

        // define a method for saving. 
        // this method is not immediately called
        // but instead can be tied to a ng-click event.
        $scope.save = function () {

            //call the save friend method as defined above
            carFactory.saveCar($scope.Vehicle)
                .success(function () {
                    // redirect the path to Routes
                    $location.path('/Home');
                })
                .error(function (data, status) {
                    alert('oh crap! status: ' + status);
                });
        };
    });

//controller for specific car 
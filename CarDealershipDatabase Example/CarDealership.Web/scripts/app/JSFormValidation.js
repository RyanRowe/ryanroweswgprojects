﻿$(document)
    .ready(function () {
        $('#requestForm')
            .validate({
                rules: {
                    CustomerName: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Phone: {
                        required: true,
                        phoneUS: true

                    },
                    TimeToCall: {
                        required: true

                    },
                    ContactMethod: {
                        required: true

                    },
                    VehicleName: {
                        required: true
                    }

    },
                messages: {
                    FirstName: {
                        required: "Enter your name"
                    },
                    Email: {
                        required: "Enter your E-mail address",
                        email: "E-mail was not in correct format"
                    },
                    Phone: {
                        required: "Enter your phone number"
                    },
                    TimeToCall: {
                        required: "Select the best time to call"
                    },
                    ContactMethod: {
                        required: "What's your preferred method of contact?"
                    },
                    VehicleName: {
                        required: "Which vehicle are you interested in?"
                    }

                }
            });
    });
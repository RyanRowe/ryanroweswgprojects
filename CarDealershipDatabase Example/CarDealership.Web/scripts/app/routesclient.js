﻿var myApp = angular.module('carPortalApp', ['ngRoute']);

myApp.factory('carFactory',
    function ($http) {
        // create a new object
        var webAPIProvider = {};

        // url of our WebAPI Controller
        var url = '/api/CarAPI/';

        // add a method to get all the cars
        webAPIProvider.getCars = function () {
            return $http.get(url);
        };

        // add a method to add a car
        webAPIProvider.saveCar = function (vehicle) {
            return $http.post(url, vehicle);
        };

        //Need method for getting specific car
        webAPIProvider.getCar = function (vehicleId) {
            return $http.post(url, vehicleId);
        };
        // return our wrapped API object
        return webAPIProvider;
    });

// this is configuring the routes that we wish to use
// in our application. 
myApp.config(function ($routeProvider) {
    // when we get to the route specified set these options.
    $routeProvider.when('/CarPortal',
        {
            controller: 'CarPortalController',
            templateUrl: '/AngularViews/CarsList.html'
        })
  
   
        // if no route is recognized do this
        .otherwise({ redirectTo: '/CarPortal' });
});

// controller for the get list.
myApp.controller('CarPortalController',
    function ($scope, carFactory) {
        // call the get friends method as defined above. 
        carFactory.getCars()
            .success(function (data) {
                // if the call succeeds set the scope 
                // variable called car.
                $scope.Vehicle = data;
            })
            .error(function (data, status) {
                alert('oh crap! status: ' + status);
            });
    });



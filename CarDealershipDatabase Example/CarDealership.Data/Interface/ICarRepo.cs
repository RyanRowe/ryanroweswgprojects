﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;

namespace CarDealership.Data.Interface
{
   public interface ICarRepo
    {
        bool Add(Vehicle car);

        List<Vehicle> GetAll();

        bool Remove(int vehicleId);

        Vehicle GetById(int vehicleId);

        bool Edit(Vehicle car);
    }
}

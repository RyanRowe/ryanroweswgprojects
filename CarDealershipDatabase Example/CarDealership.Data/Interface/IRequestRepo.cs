﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;

namespace CarDealership.Data.Interface
{
    public interface IRequestRepo
    {
        
        List<Request> GetAllRequests();

        Request GetByRequestId(int requestId);

        bool AddRequest(Request request);

        bool DeleteRequest(int requestId);

        bool EditRequest(Request request);
    }
}

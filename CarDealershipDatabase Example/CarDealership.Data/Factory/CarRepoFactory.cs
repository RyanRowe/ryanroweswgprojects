﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interface;
using CarDealership.Data.Repos;
using System.Configuration;

namespace CarDealership.Data.Factory
{
    public class CarRepoFactory
    {
        public static ICarRepo CreatCarRepo( )
        {
            ICarRepo repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString( );
            switch (mode)
            {
                case "TEST":
                    repo = new CarMockRepo( );
                    break;
                case "PROD":
                    repo = new CarDBRepo();
                    break;
                default:
                    throw new NotImplementedException( );
            }

            return repo;
        }
    }
}
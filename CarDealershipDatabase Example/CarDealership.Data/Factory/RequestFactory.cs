﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interface;
using CarDealership.Data.Repos;

namespace CarDealership.Data.Factory
{
    public class RequestFactory
    {
        public static IRequestRepo CreateRequestRepo()
        {
            IRequestRepo repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();
            switch (mode.ToUpper())
            {
                case "TEST":
                    repo = new RequestMockRepo();
                    break;
                case "PROD":
                    repo = new RequestDBRepo();
                    break; 
                default:
                    throw new NotImplementedException();

            }

            return repo;
        }
    }

}
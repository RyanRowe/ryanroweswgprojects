﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interface;
using CarDealership.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace CarDealership.Data.Repos
{
    public class CarDBRepo : ICarRepo
    {
        private static string _connectionString = ConfigurationManager.ConnectionStrings["CarDealership"].ConnectionString;



        public bool Add(Vehicle car)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "VehicleAdd";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue(@"Make", car.Make);
                cmd.Parameters.AddWithValue(@"Model", car.Model);
                cmd.Parameters.AddWithValue(@"Year", car.Year);
                cmd.Parameters.AddWithValue(@"Mileage", car.Mileage);
                cmd.Parameters.AddWithValue(@"Price", car.Price);
                cmd.Parameters.AddWithValue(@"Description", car.Description);
                cmd.Parameters.AddWithValue(@"Title", car.Title);
                cmd.Parameters.AddWithValue(@"status", car.status.GetHashCode());
                cmd.Parameters.AddWithValue(@"VehicleId", car.VehicleId);
                cmd.ExecuteNonQuery();
            }

            return true; 


        }
        public bool Edit(Vehicle car)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spVehicleEdit";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue(@"Make", car.Make);
                cmd.Parameters.AddWithValue(@"Model", car.Model);
                cmd.Parameters.AddWithValue(@"Year", car.Year);
                cmd.Parameters.AddWithValue(@"Mileage", car.Mileage);
                cmd.Parameters.AddWithValue(@"Price", car.Price);
                cmd.Parameters.AddWithValue(@"Description", car.Description);
                cmd.Parameters.AddWithValue(@"Title", car.Title);
                cmd.Parameters.AddWithValue(@"status", car.status.GetHashCode());
                cmd.Parameters.AddWithValue(@"VehicleId", car.VehicleId);
                cmd.ExecuteNonQuery();

            }

            return true;
           }


        public List<Vehicle> GetAll()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Vehicles";

                cmd.Connection = cn;

                cn.Open();

                using(var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Vehicle car = new Vehicle();

                        car = ConvertToVehicle(dr);

                        vehicles.Add(car);
                    }
                }
            }


            return vehicles;
        }

        private Vehicle ConvertToVehicle(SqlDataReader dr)
        {
            return new Vehicle()
            {
                VehicleId = (int)dr["VehicleId"],
                Make = dr["Make"].ToString(),
                Model = dr["Model"].ToString(),
                Year = (int)dr["Year"],
                Mileage = (int)dr["Mileage"],
                Price = (decimal)dr["Price"],
                Description = dr["Description"].ToString(),
                Title = dr["Title"].ToString(),
                status = (CarStatus)dr["status"]

            };
        }

        public Vehicle GetById(int vehicleId)
        {
            var vehicles = GetAll();

            var vehicle = vehicles.Where(v => v.VehicleId == vehicleId).First();

            return vehicle;
        }

        public bool Remove(int vehicleId)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spVehicleDelete";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();

                cmd.Parameters.AddWithValue(@"VehicleId", vehicleId);

                cmd.ExecuteNonQuery();

            }

            return true;

        }
    }
}

﻿using CarDealership.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace CarDealership.Data.Repos
{
    public class RequestDBRepo : IRequestRepo
    {
        private static string _connectionString = ConfigurationManager.ConnectionStrings["CarDealership"].ConnectionString;

        public bool AddRequest(Request request)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spRequestAdd";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue(@"RequestID", request.RequestID);
                cmd.Parameters.AddWithValue(@"VehicleId", request.VehicleId);
                cmd.Parameters.AddWithValue(@"CustomerName", request.CustomerName);
                cmd.Parameters.AddWithValue(@"Phone", request.Phone);
                cmd.Parameters.AddWithValue(@"Email", request.Email);
                cmd.Parameters.AddWithValue(@"TimeToCall", request.TimeToCall.GetHashCode());
                cmd.Parameters.AddWithValue(@"ContactMethod", request.ContactMethod);
                cmd.Parameters.AddWithValue(@"VehicleName", request.VehicleName);
                cmd.Parameters.AddWithValue(@"TimeFrame", request.TimeFrame);
                cmd.Parameters.AddWithValue(@"StatusOfRequest", request.StatusOfRequest);
                cmd.ExecuteNonQuery();
            }

            return true;
        }
        public bool DeleteRequest(int requestId)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spDeleteRequest";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();

                cmd.Parameters.AddWithValue(@"RequestID", requestId);

                cmd.ExecuteNonQuery();

            }

            return true;
        }

        public bool EditRequest(Request request)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spRequestEdit";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.AddWithValue(@"RequestID", request.RequestID);
                cmd.Parameters.AddWithValue(@"VehicleId", request.VehicleId);
                cmd.Parameters.AddWithValue(@"CustomerName", request.CustomerName);
                cmd.Parameters.AddWithValue(@"Phone", request.Phone);
                cmd.Parameters.AddWithValue(@"Email", request.Email);
                cmd.Parameters.AddWithValue(@"TimeToCall", request.TimeToCall.GetHashCode());
                cmd.Parameters.AddWithValue(@"ContactMethod", request.ContactMethod);
                cmd.Parameters.AddWithValue(@"VehicleName", request.VehicleName);
                cmd.Parameters.AddWithValue(@"TimeFrame", request.TimeFrame);
                cmd.Parameters.AddWithValue(@"StatusOfRequest", request.StatusOfRequest);
                cmd.ExecuteNonQuery();
            }

            return true;

        }

        public List<Request> GetAllRequests()
        {
            List<Request> requests = new List<Request>();
            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Requests";

                cmd.Connection = cn;

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Request request = new Request();

                        request = ConvertToRequest(dr);

                        requests.Add(request);
                    }
                }
            }


            return requests;
        }

        private Request ConvertToRequest(SqlDataReader dr)
        {

            return new Request()
            {
                RequestID = (int)dr["RequestID"],
                VehicleId = (int)dr["VehicleId"],
                CustomerName = dr["CustomerName"].ToString(),
                Phone = dr["Phone"].ToString(),
                Email = dr["Email"].ToString(),
                ContactMethod = dr["ContactMethod"].ToString(),
                VehicleName = dr["VehicleName"].ToString(),
                TimeToCall =(BestTimeToCall) dr["TimeToCall"].GetHashCode(),
                TimeFrame = dr["TimeFrame"].ToString(),
                StatusOfRequest = (RequestStatus) dr ["StatusOfRequest"].GetHashCode()

            };
        }

        public Request GetByRequestId(int requestId)
        {
            var requests = GetAllRequests();

            var request = requests.Where(v => v.RequestID == requestId).First();

            return request;
        }
    }
}

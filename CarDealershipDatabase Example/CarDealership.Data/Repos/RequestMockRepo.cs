﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interface;
using CarDealership.Models;

namespace CarDealership.Data.Repos
{
    public class RequestMockRepo : IRequestRepo
    {
        private static List<Request> _Requests = new List<Request>();

        static RequestMockRepo()
        {
            _Requests = new List<Request>()
            {
                new Request()
                {
                    RequestID = 1,
                    VehicleId = 2,
                    CustomerName = "Earl Gray",
                    Email = "egray@gmail.com",
                    Phone = "123-456-7890",
                    TimeToCall = BestTimeToCall.Morning,
                    ContactMethod = "E-mail",
                    VehicleName = "Jetta",
                    RequestStatus = RequestStatus.LostOpportunity
                },
                new Request()
                {
                    RequestID = 2,
                    VehicleId = 1,
                    CustomerName = "Pearl Smith",
                    Email = "psmith@gmail.com",
                    Phone = "440-486-7890",
                    TimeToCall = BestTimeToCall.Evening,
                    ContactMethod = "Phone",
                    VehicleName = "Honda",
                    RequestStatus = RequestStatus.FutureProspect
                },
                new Request()
                {
                    RequestID = 3,
                    VehicleId = 1,
                    CustomerName = "Roy Munson",
                    Email = "kingpin@gmail.com",
                    Phone = "555-486-2290",
                    TimeToCall = BestTimeToCall.Afternoon,
                    ContactMethod = "Phone",
                    VehicleName = "Toyota",
                    RequestStatus = RequestStatus.AwaitingReply
                },
            };
        }


        public List<Request> GetAllRequests()
        {
            return _Requests;
        }

        public Request GetByRequestId(int RequestID)
        {
            return _Requests.Find(r => r.RequestID == RequestID);
        }

        public bool AddRequest(Request request)
        {
            _Requests.Add(request);
            request.RequestID = _Requests.IndexOf(request);

            return true;
        }

       public bool DeleteRequest(int RequestID)
        {
            _Requests.Remove(_Requests.Find(r => r.RequestID == RequestID));

            return true;
        }

        public bool EditRequest(Request request)
        {
            var editRequest = _Requests.Find(r => r.RequestID == request.RequestID);
            _Requests.Remove(editRequest);
            _Requests.Add(request);

            return true;
        }



    }
}
    



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Data.Interface;
using CarDealership.Models;

namespace CarDealership.Data.Repos
{
    public class CarMockRepo : ICarRepo
    {
        public static List<Vehicle> _Vehicles = new List<Vehicle>();

        static CarMockRepo()
        {
            _Vehicles = new List<Vehicle>()
            {
                new Vehicle()
                {
                    VehicleId = 1,
                    Year = 2007,
                    Make = "Honda",
                    Model = "Civic",
                    Mileage = 45000,
                    Title = "Great Car!",
                    Description = "This is a great car!",
                    Price = 10000M,
                    status = CarStatus.Used
                },
                new Vehicle()
                {
                    VehicleId = 2,
                    Year = 2016,
                    Make = "Ford",
                    Model = "Mustang",
                    Mileage = 250,
                    Title = "Great Car!",
                    Description = "Another great car, brand new",
                    Price = 30000M,
                    status = CarStatus.New

                }
            };
         }  

        public bool Add(Vehicle car)
        {
            _Vehicles.Add(car);
            car.VehicleId = _Vehicles.IndexOf(car);
            return true;
        }

        public bool Edit(Vehicle car)
        {
            var carToEdit = GetById(car.VehicleId);

            _Vehicles.Remove(carToEdit);
            _Vehicles.Add(car);
            return true;

        }

        public List<Vehicle> GetAll()
        {
            return _Vehicles;
        }

        public Vehicle GetById(int vehicleId)
        {
            return _Vehicles.Find(v => v.VehicleId == vehicleId);
        }

        public bool Remove(int vehicleId)
        {
            _Vehicles.Remove(_Vehicles.Find(v => v.VehicleId == vehicleId));

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Ships;

namespace BattleShip.UI.GameView
{
    public class PlayerBoard  
    {
        //This will populate board for the ship placements- will override later for different types of boards
        public virtual char[,] PopulateBoard(Board board)
        {

            char[,] resultboard = new char[10, 10];

            for (int i = 0; i < board.Ships.Length; i++)
            {
                if (board.Ships[i] != null)
                {
                    foreach (var ship in board.Ships[i].BoardPositions)
                    {
                        resultboard[ship.XCoordinate - 1, ship.YCoordinate - 1] = 'S';

                    }
                }

            }
            return resultboard;
        }

        //Displays the board to the player- relies on populate board
        public void CreateBoard(Board board)
        {

            char[,] gameBoard = PopulateBoard(board);

            
            char[] yAxisLetters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
            
            Console.WriteLine("    1     2     3     4     5     6     7     8     9    10");
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write(" ╔═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╗");

            for (int i = 0; i < 10; i++)
            {
                Console.Write("\n{0} ", yAxisLetters[i]);

                for (int j = 0; j < 10; j++)
                {
                    

                    if(gameBoard[i,j] == 'S')
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write($"  {gameBoard[i, j]}   ");
                        Console.ResetColor();
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    else if(gameBoard[i,j] == 'H')
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"  {gameBoard[i, j]}   ");
                        Console.ResetColor();
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    else if(gameBoard[i, j] == 'M')
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write($"  {gameBoard[i, j]}   ");
                        Console.ResetColor();
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    else
                    {
                        Console.Write($"  {gameBoard[i, j]}   ");
                    }


                }
                if (i == 9)
                {
                    Console.WriteLine("\n ╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝");
                }
                else
                {
                    Console.Write("\n ╠═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╣");
                }
                Console.BackgroundColor = ConsoleColor.DarkBlue;
            }

            Console.BackgroundColor = ConsoleColor.Black;

        }

    }

}
﻿using BattleShip.BLL.GameLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.UI.GameView;

namespace BattleShip.UI
{
    public class ShotBoard : PlayerBoard
    {
        //override playerboard populateboard method, replaces with the shothistory dict in board
        public override char[,] PopulateBoard(Board board)
        {

            char[,] resultboard = new char[10, 10];

            foreach (var shot in board.ShotHistory)
            {
                char history;

                if (shot.Value == ShotHistory.Hit)
                {
                    history = 'H';
                }
                else
                {
                    history = 'M';
                }

                resultboard[shot.Key.XCoordinate - 1, shot.Key.YCoordinate - 1] = history;
            }

            return resultboard;
        }

        //inherits the createboard method from playerboard 

    }
}
            
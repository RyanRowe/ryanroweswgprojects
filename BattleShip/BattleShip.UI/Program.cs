﻿using BattleShip.UI.GameView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {

            GameWorkFlow game = new GameWorkFlow();

            game.StartGame();
            game.GameEndCredits();

            Console.ReadLine();
        }
    }
}

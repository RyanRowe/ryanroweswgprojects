﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace BattleShip.UI
{
    public class Sound
    {
        SoundPlayer soundPlayer = new System.Media.SoundPlayer();

        public void HitSound()
        {
            string[] soundCollection = new string[4];

            soundCollection[0] = @"C:\_repos\battleship\BattleShip\Sounds\sob.wav";
            soundCollection[1] = @"C:\_repos\battleship\BattleShip\Sounds\trap.wav";
            soundCollection[2] = @"C:\_repos\battleship\BattleShip\Sounds\explosion.wav";
            soundCollection[3] = @"C:\_repos\battleship\BattleShip\Sounds\no.wav";
            soundPlayer.SoundLocation = soundCollection[RandomSound()];
            soundPlayer.Play();

        }

        public void BadInputSound()
        {
            string[] soundCollection = new string[4];
            soundCollection[0] = @"C:\_repos\battleship\BattleShip\Sounds\bull.wav";
            soundCollection[1] = @"C:\_repos\battleship\BattleShip\Sounds\jabba.wav";
            soundCollection[2] = @"C:\_repos\battleship\BattleShip\Sounds\lack_discipline.wav";
            soundCollection[3] = @"C:\_repos\battleship\BattleShip\Sounds\jabba.wav";
            soundPlayer.SoundLocation = soundCollection[RandomSound()];
            soundPlayer.Play();

        }

        public void MissSound()
        {

            string[] soundCollection = new string[4];
            soundCollection[0] = @"C:\_repos\battleship\BattleShip\Sounds\bull.wav";
            soundCollection[1] = @"C:\_repos\battleship\BattleShip\Sounds\jabba.wav";
            soundCollection[2] = @"C:\_repos\battleship\BattleShip\Sounds\lack_discipline.wav";
            soundCollection[3] = @"C:\_repos\battleship\BattleShip\Sounds\poorly.wav";

            soundPlayer.SoundLocation = soundCollection[RandomSound()];
            soundPlayer.Play();
        }

        public void GameOverSound()
        {
          soundPlayer.SoundLocation = @"C:\_repos\battleship\BattleShip\Sounds\game_over.wav";
            soundPlayer.Play();
        }

        public void SwitchTurnSound()
        {
            soundPlayer.SoundLocation = @"C:\_repos\battleship\BattleShip\Sounds\logavel.wav";
            soundPlayer.Play();
        }

        public void SunkShipSound()
        {
            soundPlayer.SoundLocation = @"C:\_repos\battleship\BattleShip\Sounds\terminated.wav";
            soundPlayer.Play();
        }


        public int RandomSound()
        {
           Random rng = new Random();

           int randomSeed = rng.Next(0, 4);

           return randomSeed;
        }
       
    }
}

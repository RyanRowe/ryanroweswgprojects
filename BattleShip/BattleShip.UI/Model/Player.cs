﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class Player
    {
        public string playerName { get; set; }

        //Default constructor for player
        public Player()
        {
            playerName = "NewPlayer";

        }
        //Overload to take a name 
        public Player(string name)
        {
            playerName = name;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Ships;
using BattleShip.UI.GameView;

namespace BattleShip.UI
{
    public class InputManager
    {


        public string AskForName()
        {
            string name;
            Console.Write("Hello new Player please give me your name: ");       
            name = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(name))
            {
                Console.Clear();
                Console.WriteLine("Please Enter at least one Letter or Number for a name");
                name = Console.ReadLine();
                Console.Clear();
            }
            return name;

        }

        public string GetCoordinateFromPlayer()
        {

            string inputCoord = Console.ReadLine();
            return inputCoord;
        }

        public bool ValidateCoord(string inputCoord)
        {
            int x = 0;
            int y = 0;
            char ycord = ' ';
            inputCoord = inputCoord.ToUpper();

            if (string.IsNullOrWhiteSpace(inputCoord))
            {

                Console.WriteLine("That was an invalid coordinate, press any key to try again: ");
                Console.ReadKey();
                return false;
            }


            if (!(int.TryParse(inputCoord.Substring(1), out x) && char.TryParse(inputCoord.Substring(0, 1), out ycord)))
            {
                Console.WriteLine("That was an invalid coordinate, press any key to try again: ");
                Console.ReadKey();
                return false;
            }

            y = char.ToUpper(ycord) - 64;

            if ((x < 1 || x > 10) || (y < 1 || y > 10))
            {
                Console.WriteLine("Your choice is not on the board, press any key to try again");
                Console.ReadKey();
                return false;
            }
            else
            {
                return true;
            }
        }

        public Coordinate TranslateCoordinate(string inputCoord)
        {

            int x = 0;
            int y = 0;
            char ycord = ' ';

            int.TryParse(inputCoord.Substring(1), out x);
            char.TryParse(inputCoord.Substring(0, 1), out ycord);
            y = char.ToUpper(ycord) - 64;

            Coordinate cor = new Coordinate(y, x);
            return cor;
        }

        //Leaving in to show refactor
        //// public Coordinate GetShotCoordinate(string player)
        // {
        //     bool isValid = false;

        //     int x = 0;
        //     int y = 0;
        //     char ycord = ' ';

        //     do
        //     {
        //         Console.WriteLine($"{player} please select the coordinate to fire at");
        //         string inputCoord = Console.ReadLine();
        //         inputCoord = inputCoord.ToUpper();

        //         if (string.IsNullOrWhiteSpace(inputCoord))
        //         {
        //             Console.WriteLine("That was an invalid coordinate, please try again: ");
        //             isValid = false;
        //             Console.Clear();

        //             continue;

        //         }

        //         if (!(int.TryParse(inputCoord.Substring(1), out x) && char.TryParse(inputCoord.Substring(0, 1), out ycord)))
        //         {
        //             Console.WriteLine("That was an invalid coordinate, please try again: ");

        //             isValid = false;
        //             continue;
        //         }
        //         y = char.ToUpper(ycord) - 64;

        //         if ((x < 1 || x > 10) || (y < 1 || y > 10))
        //         {
        //             Console.WriteLine("That was an invalid coordinate, please try again: ");
        //             isValid = false;
        //         }

        //         else
        //         {
        //             isValid = true;
        //         }

        //     } while (isValid == false);

        //     Coordinate cor = new Coordinate(y, x);
        //     return cor;

        // }

        public string GetDirectionFromPlayer()
        {
            Console.Write("Please select the direction. (U)p, (D)own, (R)ight, (L)eft: ");
            string dir = Console.ReadLine().ToUpper();

            return dir;
        }

        public bool ValidateDirection(string dirInput)
        {
            dirInput = dirInput.ToUpper();

            if (string.IsNullOrWhiteSpace(dirInput))
            {
                Console.WriteLine("Hey give me something to work with! Press any key to continue");
                Console.ReadKey();
                return false;
            }
            else if (dirInput != "U" && dirInput != "D" && dirInput != "R" && dirInput != "L")
            {
                Console.WriteLine("Thats not a valid direction, press any key to continue");
                Console.ReadKey();
                return false;
            }
            else
            {
                return true;
            }
        }

        public ShipDirection TranslateDirection(string dir)
        {
            ShipDirection direction = ShipDirection.Down;

            switch (dir)
            {
                case "U":
                    direction = ShipDirection.Up;
                    break;
                case "D":
                    direction = ShipDirection.Down;
                    break;
                case "R":
                    direction = ShipDirection.Right;
                    break;
                case "L":
                    direction = ShipDirection.Left;
                    break;
                default:
                    Console.WriteLine("Please put a valid input");
                    break;
            }

            return direction;
        }


















    }

}








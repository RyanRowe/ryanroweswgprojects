﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL;
using BattleShip.BLL.GameLogic;
using BattleShip.UI.GameView;
using BattleShip.BLL.Ships;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using System.Media;

namespace BattleShip.UI
{
    public class GameWorkFlow
    {
        Player player1;
        Player player2;
        Board player1board;
        Board player2board;
        PlayerBoard playerboard;
        ShotBoard shotboard;
        private int _turnCount = 1;
        Sound sound = new Sound();

        InputManager input = new InputManager();

        //This is where we will start the game
        public void StartGame()
        {
            player1board = new Board();
            player2board = new Board();
            playerboard = new PlayerBoard();
            shotboard = new ShotBoard();

            GetPlayerNames();

            SetUpGameBoard();  //Gets placement of ships on the board

            Console.Clear();

            PlayerTurn(); // runs through player turns until someone wins


        }

        private void PlayerTurn()
        {

            ShotStatus status = new ShotStatus();

            do
            {
                if (_turnCount % 2 != 0)
                {
                    //Show shot board and get shot from player
                    sound.SwitchTurnSound();
                    shotboard.CreateBoard(player1board);
                    status = GetPlayerShot(player1, player1board);
                    _turnCount++;

                }
                else
                {
                    //Show shot board to player and get shot from player
                    sound.SwitchTurnSound();
                    shotboard.CreateBoard(player2board);
                    status = GetPlayerShot(player2, player2board);
                    _turnCount++;
                }


                if (status != ShotStatus.Victory)
                {
                    //Clears the board for the next player
                    TurnSpacer();
                }

            } while (status != ShotStatus.Victory); //Will keep looping until someone wins game

            if (_turnCount % 2 == 0)
            {
                Console.WriteLine($"{player1.playerName} won the game!");
            }
            else
            {
                Console.WriteLine($"{player2.playerName} won the game!");
            }
        }

        //Will return either hit or miss for players coordinate choice 
        public ShotStatus GetPlayerShot(Player player, Board board)
        {
            FireShotResponse response = new FireShotResponse();
            string playerInput;
            bool isValid = false;

            do //Loop until they successfully make a shot 
            {

                do // Loop until a valid coord is given
                {
                    Console.Clear();
                    shotboard.CreateBoard(board);
                    Console.WriteLine($"{player.playerName} please select a coordinate to fire at");
                    playerInput = input.GetCoordinateFromPlayer();
                    isValid = input.ValidateCoord(playerInput);


                } while (isValid == false);
                Coordinate cor = input.TranslateCoordinate(playerInput);

                response = board.FireShot(cor);

                switch (response.ShotStatus)
                {
                    case ShotStatus.Duplicate:
                        Console.WriteLine("Hey you already shot there! Press any key to try again");
                        sound.BadInputSound();
                        Console.ReadKey();
                        break;
                    case ShotStatus.Invalid:
                        Console.WriteLine("Thats not a valid coordinate! Press any key to try again");
                        sound.BadInputSound();
                        Console.ReadKey();
                        break;
                    case ShotStatus.Miss:
                        Console.WriteLine("You missed!");
                        sound.MissSound();
                        break;
                    case ShotStatus.Hit:
                        Console.WriteLine("You got a hit!");
                        sound.HitSound();
                        break;
                    case ShotStatus.HitAndSunk:
                        Console.WriteLine("You got a hit and sunk your opponents {0}!", response.ShipImpacted);
                        sound.SunkShipSound();
                        break;
                    case ShotStatus.Victory:
                        Console.WriteLine("You Won!");
                        sound.GameOverSound();
                        break;
                }

            } while (response.ShotStatus == ShotStatus.Duplicate || response.ShotStatus == ShotStatus.Invalid);
            //Will break out if the shot can be placed - otherwise goes back through the loop

            return response.ShotStatus;
        }

        //Have players pick where they are going to place there ships on the board
        public void SetUpGameBoard()
        {
            //When refactoring lets ask the player for what type of ship they want to place
            //Keep track of ships places in an array or list- switch player when they are done
            foreach (ShipType value in Enum.GetValues(typeof(ShipType)))
            {
                GetPlayerPlacement(player1, player1board, value);
            }

            foreach (ShipType value in Enum.GetValues(typeof(ShipType)))
            {
                GetPlayerPlacement(player2, player2board, value);
            }


        }

        //Places the players ships on the board 
        public void GetPlayerPlacement(Player player, Board board, ShipType type)
        {
            //instantiate a request and reponse to use in our loop
            PlaceShipRequest request = new PlaceShipRequest();
            ShipPlacement response = new ShipPlacement();

            //Loop until they successfully place the ship
            do
            {
                string ship = Enum.GetName(typeof(ShipType), type); //get string name of ship player is trying to place
                bool isValid = false;
                string playerInput; //variable used for testing players inputs 

                //Loop until valid coordinate for ship direction is given
                do
                {
                    Console.Clear();
                    playerboard.CreateBoard(board);
                    Console.WriteLine($"{player.playerName} please place your {ship}");

                    playerInput = input.GetCoordinateFromPlayer();
                    isValid = input.ValidateCoord(playerInput);

                } while (isValid == false);

                Coordinate cor = input.TranslateCoordinate(playerInput);

                //Loop until Valid direction is given
                do
                {
                    Console.Clear();
                    playerboard.CreateBoard(board);
                    Console.WriteLine("Please select the direction for your ship");
                    playerInput = input.GetDirectionFromPlayer();
                    isValid = input.ValidateDirection(playerInput);

                } while (isValid == false);

                ShipDirection dir = input.TranslateDirection(playerInput);

                //Need to validate the input that the ship can be placed on the board
                request.Coordinate = cor;
                request.Direction = dir;
                request.ShipType = type;

                //place ship on board if valid
                response = board.PlaceShip(request);

                //Give error message if not a valid placement
                switch (response)
                {
                    case (ShipPlacement.NotEnoughSpace):
                        Console.WriteLine("There is not enough space to place the ship! Press any key to try again");
                        Console.ReadKey();
                        break;
                    case (ShipPlacement.Overlap):
                        Console.WriteLine("You are overlapping another ship! Press any key to try again");
                        Console.ReadKey();
                        break;
                    default:
                        break;
                }


            } while (response != ShipPlacement.Ok); //will repeat full loop if cant place ship, otherwise ship gets placed on the board


        }


        public void GetPlayerNames()
        {
            player1 = new Player(input.AskForName());
            player2 = new Player(input.AskForName());


        }


        private void TurnSpacer()
        {

            Console.WriteLine("Press any key to clear the screen for the next player");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Press enter to start your turn- wait until your opponent looks away!");
            Console.ReadKey();
            Console.Clear();
        }


        public void PlayAgain()
        {
            bool valid = false;
            Console.Write("Would you like to play again? (Y)es or (N)o:");
            string input = Console.ReadLine();
            input = input.ToUpper();

            if (input.Substring(0, 1) == "N" || input.Substring(0, 1) == "Y")
            {

                valid = true;
            }

            while (!valid)
            {
              
                valid = false;
                Console.WriteLine("Please Enter Valid Input y for (Y)es n for (N)o");
                input = Console.ReadLine();
                input = input.ToUpper();

                if (input.Substring(0, 1) == "N" || input.Substring(0, 1) == "Y")
                {

                    valid = true;
                }

            }

            if (input.Substring(0, 1) == "Y")
            {

                player1board = new Board();
                player2board = new Board();
                playerboard = new PlayerBoard();
                shotboard = new ShotBoard();
                StartGame();


            }
            else
            {
                Console.WriteLine("Thanks for playing(Press Enter to Close)");
            }
        }

        public void GameEndCredits()
        {
            Console.WriteLine("GG thanks for playing!!! I think this calls for a rematch");
            Console.ReadKey();
            Console.Clear();
            PlayAgain();


        }
    }


}


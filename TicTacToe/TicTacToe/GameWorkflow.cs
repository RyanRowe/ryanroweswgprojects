﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{

    class GameWorkflow
    {
        Player player1 = new Player();
        Player player2 = new Player();
        Board board = new Board();
        public char[] gameBoard = new char[10] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
        GameLogic game = new GameLogic();



        public void Execute()
        {
            StartGame();
        }

        public void StartGame()
        {
            GetPlayerNames();
            Play();
        }

        public void Play()

        {


            bool winner = false;

            for (int i = 0; i < 9 && !winner; i++)

            {
                if (i % 2 == 0)
                {
                    game.PlayerTurn(player1, gameBoard);
                    winner = game.CheckForWinnners(gameBoard);

                    if (winner)
                    {
                        Console.Clear();
                        board.DrawBoard(gameBoard);
                        Console.WriteLine("Nice Job {0} you won", player1.PlayerName);
                        player1.WinCount++;
                        Console.WriteLine("The ScoreBoard is {0}: {1}, {2}: {3} (Press Enter for New Game or Close Application to Quit)", player1.PlayerName, player1.WinCount, player2.PlayerName, player2.WinCount);
                        Console.ReadKey();
                        Console.Clear();
                        gameBoard = new char[10] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                        Play();



                    }


                }
                else 
                {
                    game.PlayerTurn(player2, gameBoard);
                    winner = game.CheckForWinnners(gameBoard);
                    if (winner)
                    {
                        Console.Clear();
                        board.DrawBoard(gameBoard);
                        Console.WriteLine("Nice Job {0} you won", player2.PlayerName);
                        player2.WinCount++;
                        Console.WriteLine("The ScoreBoard is {0}: {1}, {2}: {3} (Press Enter for New Game or Close Application to Quit)", player1.PlayerName, player1.WinCount, player2.PlayerName, player2.WinCount);
                        Console.ReadKey();
                        Console.Clear();
                        gameBoard = new char[10] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                        Play();




                    }
                }
            }
            if (!winner)
            {
                Console.WriteLine("Tie Game {0} and {1}", player1.PlayerName, player2.PlayerName);
                Console.WriteLine("The ScoreBoard is {0}: {1}, {2}: {3} (Press Enter for New Game or Close Application to Quit)", player1.PlayerName, player1.WinCount, player2.PlayerName, player2.WinCount);
                Console.ReadKey();
                Console.Clear();
                gameBoard = new char[10] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                Play();

            }




        }

        public void GetPlayerNames()
        {

            Console.WriteLine($"Player One please give me a name");
            player1.PlayerName = Console.ReadLine();
            player1.PlayerToken = 'X';
            player1.WinCount = 0;


            while (string.IsNullOrWhiteSpace(player1.PlayerName))
            {
                Console.WriteLine("Please give me a valid name");
                string validNamep1 = Console.ReadLine();
                player1.PlayerName = validNamep1;
            }



            Console.WriteLine($"Player Two please give me a name");
            player2.PlayerName = Console.ReadLine();
            player2.PlayerToken = 'O';
            player2.WinCount = 0;


            while (string.IsNullOrWhiteSpace(player2.PlayerName))
            {
                Console.WriteLine("Please give me a valid name");
                string validNamep2 = Console.ReadLine();
                player2.PlayerName = validNamep2;
            }

            Console.WriteLine("Good Luck {0} & {1} (Press Enter to Start) ", player1.PlayerName, player2.PlayerName);
            Console.ReadLine();
            Console.Clear();


        }




    }
}

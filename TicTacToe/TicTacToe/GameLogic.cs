﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class GameLogic
    {
        //Player player1 = new Player();
        //Player player2 = new Player();






        public void PlayerTurn(Player player, char[] gameBoard)
        {
            Console.Clear();
            Board board = new Board();
            board.DrawBoard(gameBoard);
            Console.Write($"{player.PlayerName} please take your turn: ");
            string input = Console.ReadLine();
            int x = 0;
            bool valid = true;
            valid = (int.TryParse(input, out x));


            while (!valid || (x < 1 || x > 9) || ((gameBoard[x - 1] == 'O') || (gameBoard[x - 1] == 'X')))
            {
                Console.Clear();
                board.DrawBoard(gameBoard);
                Console.Write($"Please enter valid coord {player.PlayerName}: ");
                input = Console.ReadLine();
                //valid = ((gameBoard[x - 1] != 'O') || (gameBoard[x - 1] != 'X'));
                valid = (int.TryParse(input, out x));
                //gameBoard[x - 1] = player.PlayerToken;
                //valid = ((gameBoard[x - 1] != 'O') || (gameBoard[x - 1] != 'X'));
            }

            gameBoard[x - 1] = player.PlayerToken;
        }

        public bool CheckForWinnners(char[] gameBoard)
        {
            bool winner = false;

            for (int i = 0; i < 9; i += 3)
            {
                if ((gameBoard[i] == gameBoard[i + 1] && gameBoard[i + 1] == gameBoard[i + 2]) ||
                   (gameBoard[0] == gameBoard[4] && gameBoard[4] == gameBoard[8]) ||
                   (gameBoard[2] == gameBoard[4] && gameBoard[4] == gameBoard[6]) ||
                   (gameBoard[0] == gameBoard[3] && gameBoard[3] == gameBoard[6]) ||
                   (gameBoard[1] == gameBoard[4] && gameBoard[4] == gameBoard[7]) ||
                   (gameBoard[2] == gameBoard[5] && gameBoard[5] == gameBoard[8]))
                   winner = true;
            }

            return winner;
        }





    }

}



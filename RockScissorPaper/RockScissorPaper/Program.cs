﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockScissorPaper
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkFlow game = new WorkFlow();
            game.StartGame();
            Console.ReadLine();
        }
    }
}

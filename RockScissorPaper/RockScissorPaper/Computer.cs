﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockScissorPaper
{
    class Computer: Player
    {
        public string CompName { get; set; }
        public int WinCount { get; set; }
        public Choices CompChoice { get; set; }
    }
}

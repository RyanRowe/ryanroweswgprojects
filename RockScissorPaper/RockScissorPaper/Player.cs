﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockScissorPaper
{
    public class Player 
    {
        public string Name { get; set; }
        public int Credits { get; set; }
        public int WinCount { get; set; }
        public Choices PlayerChoice { get; set; }




        public string GetPlayerName()
        {

            Console.Write("Welcome Player to Ryan's R.P.Scissors!! Please Give me a Name: ");
            Name = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(Name))
            {
                Console.Clear();
                Console.WriteLine("Please Enter a Valid Name, anything will do");
                Name = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine($"Welcome {Name} please press any key to Start");
            Console.ReadKey();
            Console.Clear();
            return Name;

        }

        public int GetPlayerTokens()
        {
            //change credits to times to play and always have it decrement in the loop somehow.
            Console.Write("Please Choose the amount of Credits you would like to start with (10 max please):  ");
            string input = Console.ReadLine();
            int x;
            bool valid = (int.TryParse(input, out x) && (x > 0) && (x < 11));

            while (!valid)
            {
                Console.Clear();
                Console.WriteLine($"{x} is an invalid amount try again (10 Max please):  ");
                input = Console.ReadLine();
                valid = (int.TryParse(input, out x) && (x > 0) && (x < 11));
                Console.Clear();
            }

            Console.WriteLine($"{x} credits to play with, Good Luck");
            return x;









        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockScissorPaper
{
    class Logic
    {
        public Choices MakeChoice(Player player)


        {


            

            Console.WriteLine($"{player.Name} Please give me a choice (1 = Rock, 2 = Scissors, 3 = Paper)");
            string input = Console.ReadLine();
            int x = 0;
            bool Valid = (int.TryParse(input, out x));
           

            while (!Valid || (x < 1 || x > 3))
            {
                Console.Clear();
                Console.WriteLine($"{x} is an invalid Choice try again (1 = Rock, 2 = Scissors, 3 = Paper):  ");
                input = Console.ReadLine();
                Valid = (int.TryParse(input, out x));
                Console.Clear();
            }   

            //how does this class know that the enum exists???
            //switch (x)
            //{
            //    case (x == 1):
            //        player.PlayerChoice = Choices.Rock;
            //        break;

            //    case (x == 2):
            //        player.PlayerChoice = Choices.Scissors;
            //        break;

            //    default:
            //        player.PlayerChoice = Choices.Paper;
            //        break;

            //}

            if (x == 1)
            {
                player.PlayerChoice = Choices.Rock;
            }
            if (x == 2)
            {
                player.PlayerChoice = Choices.Scissors;
            }
            if (x == 3)
            {
                player.PlayerChoice = Choices.Paper;
            }

            Console.WriteLine($"{player.Name} you have chosen {player.PlayerChoice}");

            return player.PlayerChoice;
        }

        public Choices ComputerChoice(Computer computer)
        {
            // i can do an easy refactor where I can make an extra enum is Choices.enum,
            // and let player choose random with int 4, and computer always pick random,
            //and pass both objects player1 && comp/player two into same method.
           

            Random rng = new Random();


            int comp = rng.Next(1, 4);

            if (comp == 1)
            {
                computer.CompChoice = Choices.Rock;
            }
            if (comp == 2)
            {
                computer.CompChoice = Choices.Scissors;

            }
            if (comp == 3)
            {
                computer.CompChoice = Choices.Paper;
            }

            Console.WriteLine($"{computer.CompName} has chosen {computer.CompChoice}");
            return computer.CompChoice;

        }

        public int CheckForWinner(Player player, Computer computer)



            //three if's(1 if, 1 if else,1 else. Tie for if, elseif Player1 wins, else Player2/comp wins
        {
            int x = 0;

            switch (player.PlayerChoice)
            {
                case Choices.Rock:
                    switch (computer.CompChoice)
                    {
                        case Choices.Paper:
                            Console.WriteLine($"{computer.CompName} Wins");
                            x = 2;
                            break;
                        case Choices.Scissors:
                            Console.WriteLine($"{player.Name} Wins");
                            x = 1;
                            break;
                        case Choices.Rock:
                            Console.WriteLine($"{player.Name} && {computer.CompName} tied");
                            break;
                        default:
                            break;
                    }
                    break;

                case Choices.Scissors:
                    switch (computer.CompChoice)
                    {
                        case Choices.Paper:
                            Console.WriteLine($"{player.Name} Wins");
                            x = 1;
                            break;
                        case Choices.Scissors:
                            Console.WriteLine($"{player.Name} && {computer.CompName} tied");
                            break;
                        case Choices.Rock:
                            Console.WriteLine($"{computer.CompName} Wins");
                            x = 2;
                            break;
                        default:
                            break;
                    }
                    break;

                case Choices.Paper:
                    switch (computer.CompChoice)
                    {
                        case Choices.Paper:
                            Console.WriteLine($"{player.Name} && {computer.CompName} tied");
                            break;
                        case Choices.Scissors:
                            Console.WriteLine($"{computer.CompName} Wins");
                            x = 2;
                            break;
                        case Choices.Rock:
                            Console.WriteLine($"{player.Name} Wins");
                            x = 1;
                            break;
                        default:
                            break;
                    }
                    break;
            }
            //if(player.PlayerChoice == computer.CompChoice)
            //{
            //    Console.WriteLine($"{player.Name} && {computer.CompName} tied");
            //}

            //else if ((player.PlayerChoice == Choices.Rock && computer.CompChoice == Choices.Scissors) ||
            //         (player.PlayerChoice == Choices.Scissors && computer.CompChoice == Choices.Paper) ||
            //         (player.PlayerChoice == Choices.Paper && computer.CompChoice == Choices.Rock))
            //{
            //    Console.WriteLine($"{player.Name} Wins");
            //    x = 1;
            //}

            //else
            //{
            //    Console.WriteLine($"{computer.CompName} Wins");
            //    x = 2;
            //}
            return x;
            
            

            
        }

        public bool TrackOrQuit(int x, Player player, Computer computer)
        {
            bool quit = false;

            if(x == 2)
            {
                computer.WinCount++;
                player.Credits--;
                Console.WriteLine($"{player.Name} you have lost one credit and now have {player.Credits}");
             
            }
            if (x == 1)
            {
                player.WinCount++;
                player.Credits++;
                Console.WriteLine();
                Console.WriteLine($"{player.Name} you have won one credit and now have {player.Credits}");
       
            }

            Console.WriteLine($"{player.Name} Your Win Count is: {player.WinCount} and Loss Count is: {computer.WinCount}");
            Console.WriteLine($"{player.Credits} Credits remaining press enter to continue or Q to (Q)uit: ");
            string input = Console.ReadLine();
            input = input.ToUpper();
            
            if(input == "Q")
            {
                quit = true;
            }

            Console.Clear();
            return quit;

        } 

        //public bool QuitGame()
        //{
        //    bool quit = false;
        //    Console.WriteLine("Or Type Q to (Q)uit");
        //    string input = Console.ReadLine();
        //    input.ToUpper();
            
        //    if(input == "Q")
        //    {
        //        quit = true;
        //    }

        //    return quit;

        //}
    }
}

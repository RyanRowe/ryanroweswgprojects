﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockScissorPaper
{
    public class WorkFlow
    {
        Player player1 = new Player();
        Computer computer = new Computer();
        Logic logic = new Logic();



        public void StartGame()
        {
            //Player player1 = new Player();
            //Computer computer = new Computer();
            //Logic logic = new Logic();
            computer.CompName = "Virtual Player";
            computer.WinCount = 0;
            player1.WinCount = 0;
            player1.Name = player1.GetPlayerName();
            player1.Credits = player1.GetPlayerTokens();
            bool quit = false;
            do
            {
                int counter = 0;
                player1.PlayerChoice = logic.MakeChoice(player1);
                computer.CompChoice = logic.ComputerChoice(computer);
                counter = logic.CheckForWinner(player1, computer);
                quit = logic.TrackOrQuit(counter, player1, computer);
                
            } while (player1.Credits != 0 && !quit);

            Console.Clear();
            Console.WriteLine($"Game Over {player1.Name} thanks for Playing!!!!");
            Console.WriteLine($"Final Win/Loss is {player1.WinCount} - {computer.WinCount}");
        }
    }

}

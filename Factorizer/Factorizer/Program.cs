﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizer
{
    class Program
    {
        public static void Main(string[] args)
        {
            Welcome();
        }

        public static void Welcome()
        {
            int validInput;
            Console.Write("Factorizer pick a number please: ");
            string input = Console.ReadLine();
            bool inValid;
            
            do
            {
                inValid = int.TryParse(input, out validInput);
                if (!inValid || validInput < 1)
                {
                    Console.Write("Please Enter a Valid Number: ");
                    input = Console.ReadLine();
                }
               
            } while (!inValid || validInput < 1);

            
            int count = 0;
            int sum = 0;
            for (int i = 1; i < validInput; i++)
            {
                if (validInput % i == 0)
                {
                    count++;
                    Console.WriteLine(i);
                    sum = sum + i;
                }

            }

            Console.WriteLine($"{validInput} has this many factors: {count}");

            if (sum == validInput)
            {
                Console.WriteLine("This is a perfect number");
            }
            else
            {
                Console.WriteLine("This is not a perfect number");
            }
            if (sum == 1)
            {
                Console.WriteLine("This is a prime number");
            }
            else
            {
                Console.WriteLine("This is not a prime number");
            }





            Console.ReadLine();
        }

    }


}


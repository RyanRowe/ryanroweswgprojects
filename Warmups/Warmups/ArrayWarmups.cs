﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class ArrayWarmups
    {
        public bool FirstLast6(int[] numbers)
        {
            return ((numbers[0] == 6) || (numbers.Last() == 6));
        }

        public bool SameFirstLast(int[] numbers)
        {
            return ((numbers.Length > 0) && (numbers[0] == numbers.Last()));
        }

        public int[] MakePi(int n)
        {
            double pi = Math.PI;
            int[] result = new int[n];
            for (int i = 0; i < n; i++)
            {
                result[i] = (int)Math.Floor(pi);
                pi -= result[i];
                pi *= 10;
            }
            return result;
        }
        public bool CommonEnd(int[] a, int[] b)
        {
            bool ryan = false;
            if (a.Length > 0 && b.Length > 0)
            {
                if ((a[0] == b[0]) || (a.Last() == b.Last()))
                {
                    ryan = true;
                }
            }
            return ryan;
        }
        public int Sum(int[] numbers)
        {
            int sumsum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sumsum += numbers[i];

            }
            return sumsum;
        }
        public int[] RotateLeft(int[] numbers)
        {
            int y = numbers[0];
            for (int i = 1; i < numbers.Length; i++)
            {
                numbers[i - 1] = numbers[i];
            }
            numbers[numbers.Length - 1] = y;
            return numbers;

        }
        public int[] Reverse(int[] numbers)
        {
            int[] reverse = new int[numbers.Length];
            for (int i = 0; i < (numbers.Length); i++)
            {
                reverse[(reverse.Length - 1) - i] = numbers[i];
            }
            return reverse;
        }
        public int[] HigherWins(int[] num) //08
        {
            int[] newarray = new int[num.Length];

            for (int i = 0; i < num.Length; i++)
            {
                if (num[0] > num[num.Length - 1])
                {
                    newarray[i] = num[0];
                }
                if (num[0] < num[num.Length - 1])
                {
                    newarray[i] = num[num.Length - 1];
                }
            }

            return newarray;
        }

        public int[] GetMiddle(int[] a, int[] b)

        {
            int[] ryanarray = new int[2];
            ryanarray[1] = b[b.Length / 2];
            ryanarray[0] = a[b.Length / 2];
            return ryanarray;
        }

        public bool HasEven(int[] ryan)
        {
            bool even = false;
            for (int i = 0; i < ryan.Length; i++)
            {
                if (ryan[i] % 2 == 0)
                {
                    even = true;
                    break;
                }
            }
            return even;

        }
        public int[] KeepLast(int[] num)
        {
            int[] ryan = new int[num.Length * 2];
            ryan[ryan.Length - 1] = num[num.Length - 1];
            return ryan;
        }
        public bool Double23(int[] num)
        {
            bool ryan = false;
            int sum2 = 0;
            int sum3 = 0;
            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] == 2)
                {
                    sum2++;
                }
                if (num[i] == 3)
                {
                    sum3++;
                }
            }
            if (sum3 == 2 || sum2 == 2)
            {
                ryan = true;
            }
            return ryan;
        }

        public int[] Fix23(int[] num)
        {
            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] == 2 && num[i + 1] == 3)
                {
                    num[i + 1] = 0;
                }
            }
            return num;
        }
        public bool Unlucky1(int[] num)
        {
            bool ryan = false;
            for (int i = 0; i < 2; i++)
            {
                if (num[i] == 1 && num[i + 1] == 3)
                {
                    ryan = true;
                }
                if (num[num.Length - 1] - i == 3 && num[num.Length - 2] - i == 1)
                {
                    ryan = true;
                }

            }
            return ryan;

        }
        //public int[] Make2(int[] a, int[] b)
        //{

          
        //}   


    }
}


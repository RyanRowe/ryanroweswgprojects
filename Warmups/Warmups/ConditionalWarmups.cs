﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class ConditionalWarmups
    {
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile == true && bSmile == true)
            {
                return true;
            }
            if (aSmile == false && bSmile == false)
            {
                return true;
            }
            else
            {

                return false;
            }

        }
        public bool SleepingIn(bool weekday, bool vaca)
        {
            if (weekday == false || vaca == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int SumDouble(int a, int b)
        {
            if (a != b)
            {
                return a + b;
            }
            else
            {
                return (a + b) * 2;
            }
        }
        public int Diff21(int n)
        {
            if (n > 21)
            {
                return Math.Abs(n - 21) * 2;
            }
            else
            {
                return Math.Abs(n - 21);
            }
        }
        public bool ParrotTrouble(bool isTalking, int hour)

        {
            bool status = false;

            if (isTalking && hour < 7 || hour > 20)
            {
                status = true;
            }
            return status;

        }
        public bool MakeTens(int a, int b)
        {
            bool status = false;

            if (a + b == 10 || a == 10 || b == 10)
            {
                status = true;
            }
            return status;

        }
        public bool NearHundred(int a)
        {
            /*bool status = false;

            if ((a >= 90 && a <= 110) || (a >= 190 && a <= 210))
            {
                status = true;
            }
            return status;*/
            //return (a >= 90 && a <= 110) || (a >= 190 && a <= 210);
            return ((Math.Abs(200 - a) <= 10) || (Math.Abs(100 - a) <= 10));
        }
        public bool PosNeg(int a, int b, bool result)
        {
            bool status = false;
            if ((a > 0 && b < 0 && !result) ||
               (a < 0 && b > 0 && !result) ||
               (a < 0 && b < 0 && result))
            {
                status = true;
            }
            return status;
        }
        public string NotString(string str)
        {


            if ((str.Length < 3) || (str.Substring(0, 3) != "not"))
            {
                return $"not {str}";
            }
            return $"{str}";
        }

        //10
        public string MissingChar(string k, int n)
        {
            string ryan = k;
            ryan = k.Remove(n, 1);
            return ryan;

        }
        //11
        public string FrontBack(string str)
        {
            return str.Length != 1 ? ($"{str.Substring(str.Length - 1),1}{str.Substring(1, (str.Length - 2))}{str.Substring(0, 1)}") : $"{str}";
        }




    }
}






























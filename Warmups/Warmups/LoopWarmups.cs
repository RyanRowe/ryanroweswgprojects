﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class LoopWarmups
    {
        public string StringTimes(string str, int n)
        {
            string output = "";
            for (int i = 0; i < n; i++)
            {
                output += str;
            }
            return output;
        }
        public string FrontTimes(string str, int n)
        {
            string output = "";
            for (int i = 0; i < n; i++)
            {
                if (str.Length > 2)
                {
                    output += str.Substring(0, 3);
                }
                if (str.Length < 3)
                {
                    output += str;
                }

            }
            return output;
        }

        public int CountXX(string str)
        {

            int xCount = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str[i] == 'x' && str[i + 1] == 'x')
                {
                    xCount += 1;
                }
            }
            return xCount;
        }

        public bool DoubleXX(string str)
        {

            bool status = false;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if ((str[i] == 'x' && str[i + 1] != 'x'))
                {
                    status = false;
                    break;
                }
                else if ((str[i] == 'x' && str[i + 1] == 'x'))
                {
                    status = true;
                    break;
                }
                else
                {
                    status = false;
                }


            }
            return status;

        }
        //6 //6
        public string StringSplosion(string str)
        {
            string christian = "";
            for (int i = 1; i < str.Length + 1; i++)
            {
                christian += str.Substring(0, i);
            }
            return christian;
        }
        //6
        public int CountLast2(string str)
        {
            int f = -1;
            string sub = str.Substring(str.Length - 2, 2);
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 2) == sub)
                {
                    f++;
                }
            }
            return f;


        }//5
        public string EveryOther(string str)
        {
            string ryan = "";
            for (int i = 0; i < str.Length; i += 2)

            {
                ryan += str.Substring(i, 1);


            }
            return ryan;









        }

        public int Count9(int[] numbers)
        {
            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 9)
                {
                    sum++;
                }

            }
            return sum;

        }

        public bool Array123(int[] numbers)
        {
            bool result = false;

            for (int i = 0; i < numbers.Length - 2; i++)
            {

                if (numbers[i] == 1 && numbers[i + 1] == 2 && numbers[i + 2] == 3)
                {
                    result = true;
                }

            }
            return result;


        }

        public int SubStringMatch(string a, string b)
        {
            int count = 0;

            for (int i = 0; i < a.Length - 2 || i < b.Length - 2; i++)
            {
                if(a.Substring(i,1) == b.Substring(i, 1) && a.Substring(i+1, 1) == b.Substring(i+1, 1))
                {
                    count++;
                }
            }

            return count;
        }

        //public string StringXTest(string a)
        //{
        //    for(int i = 0; i < a.Length; i++)
        //    {
        //        if (a.Substring(i,1) == "x")
        //        {
        //            a.Remove();
        //        }

                
        //    }
        //    return a;
        //}
    }
}
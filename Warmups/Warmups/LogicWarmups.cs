﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class LogicWarmups
    {
        public bool GreatParty(int cigars, bool isWeekend)
        {
            return ((cigars >= 40 && isWeekend) || (cigars >= 40 && cigars <= 60));

        }
        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if ((yourStyle >= 8) || (dateStyle >= 8))
            {
                return 2;
            }
            else if ((yourStyle <= 2) || (dateStyle <= 2))
            {
                return 0;
            }
            else
            {
                return 1;
            }

        }
        public bool PlayOutside(int temp, bool isSummer)
        {
            return (((temp >= 60 && temp <= 90) && !isSummer) || ((temp >= 60 && temp <= 100) && isSummer));
        }
        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            
            if((speed < 61 && !isBirthday) || (speed < 66 && isBirthday))
            {
                return 0;
            }
            if((speed > 80 && !isBirthday) || (speed > 85 && isBirthday))
            {
                return 2;
            }
            else
            {
                return 1;
            }
            
            
        }
        public int SkipSum(int a, int b)
        {
            int sum = a + b;
            if (sum > 9 && sum < 20)
            {
                sum = 20;
            }
            return sum;
        }
        public string AlarmClock(int day, bool onVacation)
        {
            string time = "";
            if (day >= 1 && day <= 5)
            {
                time = "7:00";
            }
            if ((day >= 1 && day <=5) && onVacation)
            {
                time = "10:00";
            }
            if ((day == 0 || day > 5))
            {
                time = "10:00";
            }
            if ((day == 0 || day > 5) && onVacation)
            {
                time = "off";
            }
            return time;
            
        }
    }
}







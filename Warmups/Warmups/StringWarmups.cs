﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class StringWarmups
    {
        public string SayHi(string input)
        {
            string result = "Hello " + input + "!";
            return result;
        }

        public string Abba(string a, string b)
        {
            string result = a + b + b + a;
            return result;
        }

        public string MakeTags(string tag, string content)
        {
            return $"<{tag}>{content}</{tag}>";
        }

        public string InsertWord(string container, string word)
        {
            string leftSide = container.Substring(0, container.Length / 2);
            string rightSide = container.Substring(container.Length / 2);
            string result = leftSide + word + rightSide;
            return result;
        }

        public string MultipleEndings(string word)
        {
            string grabLastTwo = word.Substring(word.Length - 2, 2);
            string result = grabLastTwo + grabLastTwo + grabLastTwo;
            return result;
        }
        public string FirstHalf(string word)
        {
            string grabFirstHalf = word.Substring(0, word.Length / 2);
            string result = grabFirstHalf;
            return result;
        }
        public string TrimOne(string word)
        {
            string takeAwayFirstAndLastChar = word.Substring(1, word.Length - 2);
            string result = takeAwayFirstAndLastChar;
            return result;
        }
        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return b + a + b;
            }
            else
            {
                return a + b + a;
            }
        }



        public string RotateLeft2(string str)
        {
            string grabFirstTwo = str.Substring(0, 2);
            string noFirstTwo = str.Substring(2);
            string result = noFirstTwo + grabFirstTwo;
            return result;
        }
        /* public string RotateRight2(string str)
         {
             string grabLastTwo = str.Substring(str.Length - 2);
             string noLastTwo = str.Remove(str.Length - 2);
             string result = grabLastTwo + noLastTwo;
             return result;
         }*/
        public string RotateRight2(string str)
        {
            return $"{str.Substring(str.Length - 2)}{str.Remove(str.Length - 2)}";
        }

        public string TakeOne(string word, bool tof)
        {
            if (tof == true)
            {
                return word.Substring(0, 1);

            }
            else
            {
                return word.Substring(word.Length - 1);
            }
        }
        public string MiddleTwo(string str)
        {
            return $"{str.Substring((str.Length / 2) - 1, 2)}";
        }
        //13
        public bool EndsWithLy(string str)
        {
            if (str.Length >= 2)
            {
                string endingletters = str.Substring(str.Length - 2);
                if (endingletters == "ly")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }



        }
        public string FrontAndBack(string str, int n)
        {
            return $"{str.Substring(0, n)}{str.Substring(str.Length - n)}";
        }
        public string TakeTwoFromPosition(string str, int n)
        {
            if (n > str.Length - 2)
            {
                return str.Substring(0, 2);

            }
            else
            {
                return str.Substring(n, 2);
            }
        }
        public bool HasBad(string str)
        {
            if ((str.Substring(0, 3) == "bad") || (str.Substring(1, 3) == "bad"))
            {
                return true;
            }
            return false;
        }
        public string AtFirst(string str)
        {
            if (str.Length < 2)
            {
                return str + "@";
            }
            else
            {
                return $"{str.Substring(0, 2)}";
            }
        }
    }

}

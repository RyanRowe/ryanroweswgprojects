﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warmups;

namespace WarmupsTest
{
    [TestFixture]
    public class LoopWarmupsTest
    {
        LoopWarmups target = new LoopWarmups();

        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimesTest(string str, int n, string expected)
        {
            //Arrange
            LoopWarmups target = new LoopWarmups();
            //Act
            string actual = target.StringTimes(str, n);
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimesTest(string str, int n, string expected)
        {
            //Arrange
            LoopWarmups target = new LoopWarmups();
            //Act
            string actual = target.FrontTimes(str, n);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        public void CountXXTest(string str, int expected)
        {
            //Arrange
            LoopWarmups target = new LoopWarmups();
            //Act
            int actual = target.CountXX(str);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("axxvbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        public void DoubleXXTest(string str, bool expected)
        {
            //Arrange
            LoopWarmups target = new LoopWarmups();
            //Act
            bool actual = target.DoubleXX(str);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        public void StringSplosionTest(string str, string expected)
        {
            //Arrange

            //Act
            string actual = target.StringSplosion(str);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaaxx", 2)]
        public void CountLast2Test(string str, int expected)
        {
            int actual = target.CountLast2(str);
            Assert.AreEqual(expected, actual);

        }
        [TestCase("Hello", "Hlo")]
        [TestCase("Hi", "H")]
        [TestCase("Heeololeo", "Hello")]
        public void EveryOtherTest(string str, string expected)
        {
            string actual = target.EveryOther(str);
            Assert.AreEqual(expected, actual);
        }
        [TestCase(new int[] { 1, 2, 9 }, 1)]
        [TestCase(new int[] { 1, 9, 9 }, 2)]
        [TestCase(new int[] { 1, 9, 9, 3, 9 }, 3)]
        public void Count9Test(int[] numbers, int expected)
        {
            LoopWarmups lw = new LoopWarmups();
            int actual = lw.Count9(numbers);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] { 1, 1, 2, 3, 1 }, true)]
        [TestCase(new int[] { 1, 1, 2, 4, 1 }, false)]
        [TestCase(new int[] { 1, 1, 2, 1, 2, 3 }, true)]
        [TestCase(new int[] { 1, 1, 2, 1, 2, 4, 5, 7, 2, 7, 8, 9, 1, 2, 6, 3 }, false)]
        [TestCase(new int[] { 1, 1, 2, 1, 2, 2, 5, 6, 7, 2, 2, 3, 1, 2, 3 }, true)]



        public void Array123Test(int[] numbers, bool expected)
        {

            LoopWarmups lw = new LoopWarmups();

            bool actual = lw.Array123(numbers);
            Assert.AreEqual(expected, actual); 
           
            
        }

        [TestCase("xxcaazz", "xxbaaz", 3)]

        public void SubStringMatchTest(string a, string b, int expected)
        {
            LoopWarmups lw = new LoopWarmups();

            int actual = lw.SubStringMatch(a, b);

            Assert.AreEqual(expected, actual);

        }

        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]

        public void StringXTest(string a, string expected)
        {
            LoopWarmups lw = new LoopWarmups();

            string actual = lw.StringXTest(a);

            Assert.AreEqual(expected, actual);
        }



    }

}

﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warmups;

namespace WarmupsTests
{
    [TestFixture]
    public class LogicWarmupTests
    {
        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void GreatPartytest(int cigars, bool isWeekend, bool expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            bool actual = target.GreatParty(cigars, isWeekend);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5, 5, 1)]
        public void CanHazTableTest(int yourStyle, int dateStyle, int expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            int actual = target.CanHazTable(yourStyle, dateStyle);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        //3
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void PlayOutsideTest(int temp, bool isSummer, bool expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            bool actual = target.PlayOutside(temp, isSummer);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeedingTest(int speed, bool isBirthday, int expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            int actual = target.CaughtSpeeding(speed, isBirthday);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void SkipSumTest(int a, int b, int expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            int actual = target.SkipSum(a, b);
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        [TestCase(6, true, "off")]
        public void AlarmClockTest(int day, bool onVacation, string expected)
        {
            //Arrange
            LogicWarmups target = new LogicWarmups();
            //Act
            string actual = target.AlarmClock(day, onVacation);
            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}


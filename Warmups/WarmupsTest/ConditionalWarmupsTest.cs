﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalWarmupsTest
    {
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(false, true, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.AreWeInTrouble(aSmile, bSmile);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        public void SleepingInTest(bool weekday, bool vaca, bool expected)
        {

            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.SleepingIn(weekday, vaca);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(1, 2, 3)]
        [TestCase(3, 2, 5)]
        [TestCase(2, 2, 8)]
        public void SumDoubleTest(int a, int b, int expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            int actual = target.SumDouble(a, b);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21Test(int n, int expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            int actual = target.Diff21(n);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTroubleTest(bool isTalking, int hour, bool expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.ParrotTrouble(isTalking, hour);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void MakeTensTest(int a, int b, bool expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.MakeTens(a, b);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundredTest(int a, bool expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.NearHundred(a);
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNegTest(int a, int b, bool result, bool expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            bool actual = target.PosNeg(a, b, result);
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotStringTest(string str, string expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            string actual = target.NotString(str);
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingCharTest(string k, int n, string expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            string actual = target.MissingChar(k, n);
            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FrontBackTest(string str, string expected)
        {
            //Arrange
            ConditionalWarmups target = new ConditionalWarmups();
            //Act
            string actual = target.FrontBack(str);
            //Assert
            Assert.AreEqual(expected, actual);
        }


    }
}
﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Warmups.Tests
{
    [TestFixture]
    public class StringWarmupstest
    {
        [TestCase("Bob", "Hello Bob!")]
        public void SayHelloTest(string input, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string actual = target.SayHi(input);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string s1, string s2, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string actual = target.Abba(s1, s2);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "hello", "<i>hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void MakeTagsTest(string tag, string name, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string actual = target.MakeTags(tag, name);
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "World", "[[World]]")]
        public void InsertWordTest(string container, string word, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();

            //Act
            string result = target.InsertWord(container, word);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndingsTest(string word, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.MultipleEndings(word);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalfTest(string word, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.FirstHalf(word);
            //Assert
            Assert.AreEqual(expected, result);

        }
        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneTest(string word, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.TrimOne(word);
            //Assert
            Assert.AreEqual(expected, result);


        }
        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddleTest(string a, string b, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.LongInMiddle(a, b);
            //Assert
            Assert.AreEqual(expected, result);

        }
        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateLeft2Test(string str, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.RotateLeft2(str);
            //Assert
            Assert.AreEqual(expected, result);

        }



        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateRight2Test(string str, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.RotateRight2(str);
            //Assert
            Assert.AreEqual(expected, result);

        }
        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOneTest(string word, bool tof, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.TakeOne(word, tof);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwoTest(string word, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.MiddleTwo(word);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void EndsWithLyTest(string str, bool expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            bool result = target.EndsWithLy(str);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBackTest(string str, int n, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.FrontAndBack(str, n);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        [TestCase("jav", 1, "av")]
        public void TakeTwoFromPositionTest(string str, int n, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.TakeTwoFromPosition(str, n);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBadTest(string str, bool expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            bool result = target.HasBad(str);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirstTest(string str, string expected)
        {
            //Arrange
            StringWarmups target = new StringWarmups();
            //Act
            string result = target.AtFirst(str);
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}







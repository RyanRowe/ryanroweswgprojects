USE BowlingBlog

CREATE TABLE [dbo].[BlogPosts]
(
[BlogId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[Approval] [int] NULL,
[BlogDate] [datetime] NOT NULL,
[BlogTitle] [varchar](50) NOT NULL,
[BlogPost] [varchar](max) NOT NULL,   
)

CREATE TABLE [dbo].[Products]
(
[ProductId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[ProductName] [varchar](50) NOT NULL,
[ProductLink] [varchar](max) NOT NULL,
[ProductPrice] [decimal] NOT NULL 
)

CREATE TABLE [dbo].[Appearances]
(
[AppearanceId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[GuestName] [varchar](75) NOT NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL,
[Price] [decimal] NOT NULL
)

CREATE TABLE [dbo].[Events]
(
[EventId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[EventName] [varchar](50) NOT NULL,
[EventStartTime] [datetime] NOT NULL,
[EventEndTime] [datetime] NOT NULL,
[Event Description] [varchar](max) NOT NULL
)

CREATE TABLE [dbo].[Tags]
(
[TagId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[TagName] [varchar](50) NOT NULL
)

CREATE TABLE [dbo].[BlogsTags]
(
[BlogId] [int] NOT NULL,
[TagId] [int] NOT NULL
CONSTRAINT PK_BlogsTags PRIMARY KEY(BlogId, TagId)
)

ALTER TABLE [dbo].[BlogsTags]
	ADD CONSTRAINT FK_BlogsTags_Blogs FOREIGN KEY (BlogId)
	REFERENCES [dbo].[BlogPosts] (BlogId)

ALTER TABLE [dbo].[BlogsTags]
	ADD CONSTRAINT FK_BlogsTags_Tags FOREIGN KEY (TagId)
	REFERENCES [dbo].[Tags] (TagId)
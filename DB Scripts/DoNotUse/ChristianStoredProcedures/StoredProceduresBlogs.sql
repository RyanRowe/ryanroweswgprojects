Select Tags.TagId, Tags.TagName
From BlogPosts
Inner Join BlogsTags on BlogPosts.BlogId = BlogsTags.BlogId
Inner Join Tags on Tags.TagId = BlogPosts.BlogId

insert into BlogsTags(BlogId, TagId)
Values (1,2),
		(1,3)
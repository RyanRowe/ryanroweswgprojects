CREATE PROCEDURE GetTagsByBlogId
(
@BlogId int
)
AS
Select Tags.TagId,Tags.TagName
FROM BlogPosts
INNER JOIN BlogsTags on @BlogId = BlogsTags.BlogId
INNER JOIN Tags on BlogsTags.TagId = Tags.TagId

GO

--exec GetTagsByBlogId 1
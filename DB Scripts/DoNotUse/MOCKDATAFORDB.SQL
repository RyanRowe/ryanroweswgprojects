use BowlingBlog

INSERT INTO [dbo].[BlogPosts] (BlogDate, Approval, BlogTitle, Category, BlogPost, TagPlaceholder)
VALUES ('12/12/12', 0, 'Conshructrr', 0, 'I love Conshructrrs', 'tag1,tag2,tag3,tag4')

SELECT * 
FROM BLOGPOSTS

INSERT INTO [dbo].[Tags] (TagName)
VALUES ('tag1')

INSERT INTO [dbo].[BlogsTags](BlogId, TagId)
VALUES (1, 1)

SELECT *
FROM BlogsTags

INSERT INTO [dbo].[Contact](Name, Phone, Email, SubmissionDate, [Subject], [Message])
VALUES ('Dick Prickles','440-213-5510','dick@email.com', '12/04/2016', 'I need information on having a bowling party', 'I want to have my 100th birthday party at WhileTrue Lanes') 

SELECT *
FROM Contact

INSERT INTO [dbo].[Products](AddDate, ProductName, [Description], ProductLink, ProductPrice)
VALUES ('10/15/2016', 'Columbia 300 PinKicker', 'Maximum performance ball for intense oil patterns, the PinKicker really takes care of business', 'www.columbia.com' ,200.00)  

INSERT INTO [dbo].[Event](EventType, EventName, EventStartTime, EventEndTime, [EventDescription])
VALUES (1,'Roy Munson Book Signing','01/20/2017 04:00PM', '01/20/2017 06:00PM', 'Roy Munson will be signing his new book: 1001 ways to use your prosthetic hand. Expect long wait times.')

SELECT *
FROM Event



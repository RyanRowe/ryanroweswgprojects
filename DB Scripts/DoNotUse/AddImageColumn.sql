USE [master]
GO
/****** Object:  Database [BowlingBlog]    Script Date: 12/6/2016 10:28:47 AM ******/
CREATE DATABASE [BowlingBlog]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BowlingBlog', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BowlingBlog.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BowlingBlog_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BowlingBlog_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BowlingBlog] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BowlingBlog].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BowlingBlog] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BowlingBlog] SET ARITHABORT OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BowlingBlog] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BowlingBlog] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BowlingBlog] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BowlingBlog] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BowlingBlog] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BowlingBlog] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BowlingBlog] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BowlingBlog] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BowlingBlog] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BowlingBlog] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BowlingBlog] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BowlingBlog] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BowlingBlog] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BowlingBlog] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BowlingBlog] SET  MULTI_USER 
GO
ALTER DATABASE [BowlingBlog] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BowlingBlog] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BowlingBlog] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BowlingBlog] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BowlingBlog] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BowlingBlog]
GO
/****** Object:  Table [dbo].[BlogPosts]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BlogPosts](
	[BlogId] [int] IDENTITY(1,1) NOT NULL,
	[BlogDate] [datetime] NOT NULL,
	[Approval] [int] NULL,
	[BlogTitle] [varchar](50) NOT NULL,
	[Category] [int] NOT NULL,
	[BlogPost] [varchar](max) NOT NULL,
	[TagPlaceholder] [nvarchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BlogsTags]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogsTags](
	[BlogId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
 CONSTRAINT [PK_BlogsTags] PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC,
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](25) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[SubmissionDate] [datetime] NOT NULL,
	[Subject] [nvarchar](max) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventType] [int] NOT NULL,
	[EventName] [varchar](100) NOT NULL,
	[EventStartTime] [datetime] NOT NULL,
	[EventEndTime] [datetime] NOT NULL,
	[EventDescription] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Products]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[AddDate] [datetime] NOT NULL,
	[ProductName] [varchar](100) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ProductLink] [nvarchar](250) NOT NULL,
	[ProductPrice] [money] NOT NULL,
	[Image] [nchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tags]    Script Date: 12/6/2016 10:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tags](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[BlogPosts] ON 

INSERT [dbo].[BlogPosts] ([BlogId], [BlogDate], [Approval], [BlogTitle], [Category], [BlogPost], [TagPlaceholder]) VALUES (1, CAST(N'2012-12-12 00:00:00.000' AS DateTime), 0, N'Conshructrr', 0, N'I love Conshructrrs', N'tag1,tag2,tag3,tag4')
SET IDENTITY_INSERT [dbo].[BlogPosts] OFF
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (1, 1)
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([Id], [Name], [Phone], [Email], [SubmissionDate], [Subject], [Message]) VALUES (1, N'Dick Prickles', N'440-213-5510', N'dick@email.com', CAST(N'2016-12-04 00:00:00.000' AS DateTime), N'I need information on having a bowling party', N'I want to have my 100th birthday party at WhileTrue Lanes')
SET IDENTITY_INSERT [dbo].[Contact] OFF
SET IDENTITY_INSERT [dbo].[Event] ON 

INSERT [dbo].[Event] ([EventId], [EventType], [EventName], [EventStartTime], [EventEndTime], [EventDescription]) VALUES (5, 1, N'Roy Munson Book Signing', CAST(N'2017-01-20 16:00:00.000' AS DateTime), CAST(N'2017-01-20 18:00:00.000' AS DateTime), N'Roy Munson will be signing his new book: 1001 ways to use your prosthetic hand. Expect long wait times.')
SET IDENTITY_INSERT [dbo].[Event] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductId], [AddDate], [ProductName], [Description], [ProductLink], [ProductPrice], [Image]) VALUES (3, CAST(N'2016-12-06 10:25:46.163' AS DateTime), N'Brunswick Soul Mate', N'I''m your soul mate, buy me now.', N'http://www.bowlingball.com/products/bowling-balls/brunswick/11949/soul-mate.html', CAST(90 AS Decimal(18, 0)), N'http://i.imgur.com/z0BuZE0.jpg                                                                                                                                                                                                                            ')
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[Tags] ON 

INSERT [dbo].[Tags] ([TagId], [TagName]) VALUES (1, N'tag1')
SET IDENTITY_INSERT [dbo].[Tags] OFF
ALTER TABLE [dbo].[BlogsTags]  WITH CHECK ADD  CONSTRAINT [FK_BlogsTags_Blogs] FOREIGN KEY([BlogId])
REFERENCES [dbo].[BlogPosts] ([BlogId])
GO
ALTER TABLE [dbo].[BlogsTags] CHECK CONSTRAINT [FK_BlogsTags_Blogs]
GO
ALTER TABLE [dbo].[BlogsTags]  WITH CHECK ADD  CONSTRAINT [FK_BlogsTags_Tags] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tags] ([TagId])
GO
ALTER TABLE [dbo].[BlogsTags] CHECK CONSTRAINT [FK_BlogsTags_Tags]
GO
USE [master]
GO
ALTER DATABASE [BowlingBlog] SET  READ_WRITE 
GO

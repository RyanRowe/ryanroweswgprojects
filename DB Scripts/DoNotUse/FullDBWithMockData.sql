USE [master]
GO

if exists (select * from sysdatabases where name='BowlingBlog')
		drop database [BowlingBlog]
go

/****** Object:  Database [BowlingBlog]    Script Date: 11/28/2016 3:55:24 PM ******/
CREATE DATABASE [BowlingBlog]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BowlingBlog', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BowlingBlog.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BowlingBlog_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\BowlingBlog_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BowlingBlog] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BowlingBlog].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BowlingBlog] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BowlingBlog] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BowlingBlog] SET ARITHABORT OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BowlingBlog] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BowlingBlog] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BowlingBlog] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BowlingBlog] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BowlingBlog] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BowlingBlog] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BowlingBlog] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BowlingBlog] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BowlingBlog] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BowlingBlog] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BowlingBlog] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BowlingBlog] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BowlingBlog] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BowlingBlog] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BowlingBlog] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BowlingBlog] SET  MULTI_USER 
GO
ALTER DATABASE [BowlingBlog] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BowlingBlog] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BowlingBlog] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BowlingBlog] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BowlingBlog] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BowlingBlog]
GO
/****** Object:  Table [dbo].[Appearances]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Appearances](
	[AppearanceId] [int] IDENTITY(1,1) NOT NULL,
	[GuestName] [varchar](75) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AppearanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BlogPosts]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogPosts](
	[BlogId] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NULL,
	[BlogDate] [datetime] NOT NULL,
	[BlogTitle] [nvarchar](max) NULL,
	[BlogPost] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BlogsTags]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogsTags](
	[BlogId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
 CONSTRAINT [PK_BlogsTags] PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC,
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Events]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventName] [nvarchar](max) NULL,
	[EventStartTime] [datetime] NOT NULL,
	[EventEndTime] [datetime] NOT NULL,
	[EventDescription] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](max) NULL,
	[ProductLink] [nvarchar](max) NULL,
	[ProductPrice] [decimal](18, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tags]    Script Date: 11/28/2016 3:55:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tags](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Appearances] ON 

INSERT [dbo].[Appearances] ([AppearanceId], [GuestName], [StartTime], [EndTime], [Price]) VALUES (1, N'Clarence Cleangame Clemmins', CAST(N'2016-11-20 17:00:00.000' AS DateTime), CAST(N'2016-11-20 19:00:00.000' AS DateTime), CAST(25 AS Decimal(18, 0)))
INSERT [dbo].[Appearances] ([AppearanceId], [GuestName], [StartTime], [EndTime], [Price]) VALUES (2, N'Big Ern McKracken', CAST(N'2016-11-23 18:00:00.000' AS DateTime), CAST(N'2016-11-23 21:00:00.000' AS DateTime), CAST(75 AS Decimal(18, 0)))
INSERT [dbo].[Appearances] ([AppearanceId], [GuestName], [StartTime], [EndTime], [Price]) VALUES (3, N'The Dude', CAST(N'2016-11-28 16:00:00.000' AS DateTime), CAST(N'2016-11-28 20:00:00.000' AS DateTime), CAST(100 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Appearances] OFF
SET IDENTITY_INSERT [dbo].[BlogPosts] ON 

INSERT [dbo].[BlogPosts] ([BlogId], [Approval], [BlogDate], [BlogTitle], [BlogPost]) VALUES (2, 4, CAST(N'2015-06-19 10:03:06.000' AS DateTime), N'Must Have Balls for 2017', N'I have gathered my list of the top 5 balls to get for 2017. All of the balls on my pick list have many traits in common, they are all round, they are all heavy and they all have holes but their cores differ exponentially! Coming in at #5 is the Brunswick- Fanciful Sportsman which features a Symmetric Core and a fanciful new look.')
INSERT [dbo].[BlogPosts] ([BlogId], [Approval], [BlogDate], [BlogTitle], [BlogPost]) VALUES (3, 6, CAST(N'2015-06-20 10:03:06.000' AS DateTime), N'Controlling Your Speed', N'Tips for controlling your bowling speed: approach fast and throw hard when you want to throw the ball fast. Approach slowly and release like your tossing an egg to throw slowly... ')
INSERT [dbo].[BlogPosts] ([BlogId], [Approval], [BlogDate], [BlogTitle], [BlogPost]) VALUES (4, 5, CAST(N'2015-06-21 10:03:06.000' AS DateTime), N'Bowling Form and Technique', N'There are several unique techniques you can use to improve your game. Some pro''s roll the ball from the hip while others prefer to loft the ball. Personally I have found some major benefits to the Granny shot, it takes the approach and loft out of the equation so you can focus on perfect form and speed.')
SET IDENTITY_INSERT [dbo].[BlogPosts] OFF
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (2, 1)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (2, 2)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (2, 4)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (3, 1)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (3, 3)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (4, 1)
INSERT [dbo].[BlogsTags] ([BlogId], [TagId]) VALUES (4, 3)
SET IDENTITY_INSERT [dbo].[Events] ON 

INSERT [dbo].[Events] ([EventId], [EventName], [EventStartTime], [EventEndTime], [EventDescription]) VALUES (1, N'Midnight Bowling', CAST(N'2016-12-01 00:00:00.000' AS DateTime), CAST(N'2016-12-01 03:00:00.000' AS DateTime), N'Open bowling for up to 4 players per lane, glow lanes and a free pitcher of Pepsi.')
INSERT [dbo].[Events] ([EventId], [EventName], [EventStartTime], [EventEndTime], [EventDescription]) VALUES (2, N'Early Bird Bowling', CAST(N'2016-12-04 08:00:00.000' AS DateTime), CAST(N'2016-12-04 12:00:00.000' AS DateTime), N'Open bowling for up to 4 players per lane before Noon. Free coffee beverage.')
INSERT [dbo].[Events] ([EventId], [EventName], [EventStartTime], [EventEndTime], [EventDescription]) VALUES (3, N'Kiddie Bowling', CAST(N'2016-12-15 14:00:00.000' AS DateTime), CAST(N'2016-12-15 17:00:00.000' AS DateTime), N'Open bowling for children 13 and under, pictures with Santa, bumper bowling available.')
SET IDENTITY_INSERT [dbo].[Events] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductLink], [ProductPrice]) VALUES (1, N'Moxy Triple Ball Roller', N'https://www.moxybowling.com/product/triple-roller/', CAST(130 AS Decimal(18, 0)))
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductLink], [ProductPrice]) VALUES (2, N'Columbia 300 Tyrant Ball', N'http://www.bowlersmart.com/columbia-300-tyrant-bowling-ball', CAST(165 AS Decimal(18, 0)))
INSERT [dbo].[Products] ([ProductId], [ProductName], [ProductLink], [ProductPrice]) VALUES (3, N'Hammer Men''s Force Shoe', N'http://www.bowlersmart.com/hammer-mens-force-black-orange-right-hand-bowling-shoes', CAST(170 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[Tags] ON 

INSERT [dbo].[Tags] ([TagId], [TagName]) VALUES (1, N'Bowling')
INSERT [dbo].[Tags] ([TagId], [TagName]) VALUES (2, N'Pins')
INSERT [dbo].[Tags] ([TagId], [TagName]) VALUES (3, N'Technique')
INSERT [dbo].[Tags] ([TagId], [TagName]) VALUES (4, N'Products')
SET IDENTITY_INSERT [dbo].[Tags] OFF
ALTER TABLE [dbo].[BlogsTags]  WITH CHECK ADD  CONSTRAINT [FK_BlogsTags_Blogs] FOREIGN KEY([BlogId])
REFERENCES [dbo].[BlogPosts] ([BlogId])
GO
ALTER TABLE [dbo].[BlogsTags] CHECK CONSTRAINT [FK_BlogsTags_Blogs]
GO
ALTER TABLE [dbo].[BlogsTags]  WITH CHECK ADD  CONSTRAINT [FK_BlogsTags_Tags] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tags] ([TagId])
GO
ALTER TABLE [dbo].[BlogsTags] CHECK CONSTRAINT [FK_BlogsTags_Tags]
GO
USE [master]
GO
ALTER DATABASE [BowlingBlog] SET  READ_WRITE 
GO

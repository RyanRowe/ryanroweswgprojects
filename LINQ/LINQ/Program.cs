﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            // PrintOutOfStock();
            //InStockMoreThan3();
            //Exercise3();
            //Exercise4();
            //Exercise5();
            // Exercise6();
            // Exercise7();
            //Exercise8();
            //  Exercise9();
            // Exercise10();
            //Exercise11();
            // Exercise12();
            // Exercise13();
            // Exercise14();
            //Exercise15();
            //Exercise16();
            // Exercise17();
            // Exercise18();
            //Exercise19();
           // Exercise20();
            Console.ReadLine();
        }

        //1. Find all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock == 0);
            var results = from p in products
                          where p.UnitsInStock == 0
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        //2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products

                          where p.UnitPrice > 3.00m

                          select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }

        }

        // 4 Create a new sequence with just the names of the products.

        private static void Exercise4()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(p => new { p.ProductName });

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }

        }
        //Create a new sequence of products and unit prices where the unit prices are increased by 25%.

        private static void Exercise5()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products

                          select new { p, RaisedPrice = p.UnitPrice * 1.25M };


            foreach (var product in results)
            {
                Console.WriteLine($"{product.p.ProductName} = {product.p.UnitPrice} is now {product.RaisedPrice}");
            }
        }
        //Create a new sequence of just product names in all upper case.

        private static void Exercise6()
        {

            var products = DataLoader.LoadProducts();

            var results = from p in products

                          select new { p.ProductName };

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName.ToUpper());
            }


        }

        private static void Exercise7()
        {

            var products = DataLoader.LoadProducts();

            var results = from p in products

                          where p.UnitsInStock % 2 == 0 && p.UnitsInStock != 0

                          select new { p.ProductName, p.UnitsInStock };


            foreach (var product in results)
            {
                Console.WriteLine($"{product.ProductName} has an even amount of {product.UnitsInStock} in stock");
            }


        }

        private static void Exercise8()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products

                         select new { Price = p.UnitPrice, p.ProductName, p.Category };

            foreach (var product in result)
            {
                Console.WriteLine($"{product.ProductName} - {product.Category} - {product.Price}");
            }


        }

        private static void Exercise10()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers
                         from o in c.Orders
                         where o.Total < 500.00m

                         select new { c.CustomerID, o.OrderID, o.Total };

            foreach (var customer in result)
            {
                Console.WriteLine($"{customer.CustomerID}, {customer.OrderID}, {customer.Total}");
            }
        }


        public static void Exercise12()
        {
            var customers = DataLoader.LoadCustomers();

            var result = (from c in customers

                          from o in c.Orders

                          where c.Region == "WA"

                          select new { c.CustomerID, o.OrderID }).Take(3);

            foreach (var orders in result)
            {
                Console.WriteLine($"The First Three Orders are from {orders.CustomerID} \n \t Order #: {orders.OrderID}");

            }



        }

        public static void Exercise3()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers

                         from o in c.Orders

                         where c.Region == "WA"

                         select new { c.CustomerID, o.OrderID };

            foreach (var customer in result)
            {
                Console.WriteLine($" name - {customer.CustomerID}, orders - {customer.OrderID}");
            }

        }

        public static void Exercise11()
        {
            var numbers = DataLoader.NumbersA;

            var result = (from n in numbers

                          select n).Take(3);

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }
        }

        public static void Exercise13()
        {
            var numbers = DataLoader.NumbersA;

            var results = (from n in numbers

                           select n).Skip(3);

            foreach (var number in results)
            {
                Console.WriteLine(number);
            }



        }

        public static void Exercise14()
        {
            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers

                           from o in c.Orders

                           where c.Region == "WA"

                           select new { o.OrderID, o.Total }).Skip(2);

            foreach (var order in results)
            {
                Console.WriteLine($"Order ID # {order.OrderID} - \n\tOrder Total {order.Total}");
            }
        }


        public static void Exercise15()
        {
            var numbers = DataLoader.NumbersC.TakeWhile(num => num < 6);

            var results = from n in numbers

                          select n;

            foreach (var number in results)
            {
                //if(number > 5)
                //{
                //    break;
                //}

                Console.WriteLine(number);

            }
        }

        //16. Return elements starting from the beginning of NumbersC until a number is hit that is less than its position in the array.


        public static void Exercise16()
        {
            var numbers = DataLoader.NumbersC;

            var results = numbers.TakeWhile((n, index) => n >= index);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }







        }

        private static void Exercise9()
        {
            var numbersb = DataLoader.NumbersB;
            var numbersc = DataLoader.NumbersC;

            //int i = -1;
            //var results = from b in numbersb
            //              let index = i++
            //              where b < numbersc[i]
            //              select new { b, c = numbersc[i] };

            var results = numbersb.Select((b, i) => new { b, c = numbersc[i] }).Where(x => x.b < x.c);

            foreach (var result in results)
            {
                Console.WriteLine($"{result.b} - {result.c}");
            }




        }

        private static void Exercise17()
        {
            var numbers = DataLoader.NumbersC;

            var results = numbers.SkipWhile(num => num % 3 != 0);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        private static void Exercise18()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products.OrderBy(p => p.ProductName)

                          select p;

            foreach (var result in results)
            {
                Console.WriteLine(result.ProductName);
            }


        }

        private static void Exercise19()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products.OrderByDescending(p => p.UnitsInStock)

                          select p;

            foreach (var result in results)
            {
                Console.WriteLine($"{result.ProductName} has {result.UnitsInStock} units in stock!!");
            }

        }


        //private static void Exercise20()
        //{
        //    var products = DataLoader.LoadProducts();

        //    var results = from p in products

        //        orderby p.Category, p.UnitPrice descending

        //        select p;

        //    foreach (var result in results)
        //    {
        //        Console.Write($"{result.Category}");

        //        foreach ( in result.Category)
        //        {
        //            Console.WriteLine($"{result.ProductName}\n {result.UnitPrice:c}");

        //        }

        //}





        //foreach (var result in results)
        //{


        //}







        //foreach (var result in results)
        //{
        //    Console.WriteLine();
        //}




    }








}


















































